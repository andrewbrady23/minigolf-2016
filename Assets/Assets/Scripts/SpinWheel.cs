﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class SpinWheel : MonoBehaviour {


	DateTime currentDate;
	DateTime oldDate;

	public GameObject wheelPrefab;

	public float wheelSpeed;
	float speed;

	public GameObject resultsMenuObject;
	public Texture[] rewardTextures = new Texture[4]; // 0 = par, 1 = eraser, 2 = holeskip, 3 = unlock a level

	Image openButtonImage;
	Color openButtonColour = Color.white; 
	bool openButtonFadeDir=false;
	Vector3 buttonMoveTarget;
	Vector3 startPos;

	public Button spinWheelButton;
	public GameObject noSpinsWarning;

	private bool canPlayToday_ = false;
	public bool canPlayToday {
		get {
			if (PlayerPrefs.HasKey ("LastTimeSpunWheel")) {
				int numSpins = PlayerPrefs.GetInt ("NumSpins");
//				Debug.Log ("Number of Spins: " + numSpins);
			
				currentDate = System.DateTime.Now;
				DateTime oldDate = DateTime.Parse (PlayerPrefs.GetString ("LastTimeSpunWheel"));
			
				TimeSpan difference = currentDate.Subtract (oldDate);

			//	Debug.Log ("Minutes Since Last Spin: " + difference.Minutes);
				if (difference.Hours >= 20) {
					numSpins = 0;
					PlayerPrefs.SetInt ("NumSpins",numSpins);
				} 

				canPlayToday_ = false;



				if (numSpins == 0) {
					canPlayToday_ = true;
				}
				else if 	(numSpins == 1 && difference.Minutes >= 5) {
					canPlayToday_ = true;
				} else if (numSpins == 2 && difference.Minutes >= 10) {
					canPlayToday_ = true;
				} else if (numSpins == 3 && difference.Minutes >= 20) {
					canPlayToday_ = true;
				} else if (numSpins == 4 && difference.Minutes >= 40) {
					canPlayToday_ = true;
				} else if (difference.Minutes >= 60) {
					canPlayToday_ = true;
				} 
			} else {
				canPlayToday_ = true;
			//	StartCoroutine ("SetCanPlay");
			}

			return canPlayToday_;
		}
		set {
			if (value) {
				StartCoroutine ("SetCanPlay");
			} else {
				canPlayToday_ = false;
			}
		}
	}

	public Transform wheel;
	public Transform wheelParent;
	float wheelRot = 0;
	Vector3 rot = Vector3.zero;

	bool slideInWheel = false;
	bool done = false;
	bool isSpinning = false;
	bool hasStopped = false;

	public GameObject pointParent;
	SpinWheelPoint[] points;
	public GameObject pointer;
	public Transform pointerPoint;

	public Transform spinButton;
	Vector3 spinButtonDifference;

	public GameObject gameMenu;

	public void Enable() {
		if (canPlayToday) {
			gameObject.SetActive (true);
		}
	}

	void Start () {

#if UNITY_ANDROID || UNITY_IOS
        GameObject.Find ("InAppPurchasingController").GetComponent<IAppPurchaser>().spinner = gameObject;
#endif
		pointerPoint = pointer.transform.GetChild (0);

		gameMenu.SetActive (true);

		startPos = transform.localPosition;

		points = new SpinWheelPoint[pointParent.transform.childCount];
		for (int i = 0; i < pointParent.transform.childCount; i++) {
			points [i] = pointParent.transform.GetChild (i).GetComponent<SpinWheelPoint> () as SpinWheelPoint;
		}

		openButtonImage = GetComponent<Image> () as Image;

		buttonMoveTarget = transform.position;
		buttonMoveTarget.x = Screen.width - (Screen.width/5);


		Vector2 posForSpinButton;
		Vector2 posForAdButton;

		float x = Screen.width/2.5f;
		float y = Screen.height / 2;
		posForSpinButton = new Vector2 (x,y);
		spinButtonDifference.x = x;
		//x = Screen.width / 4;
		//posForAdButton = new Vector2 (x,y);


	//	float x = Screen.width / 3; float y = Screen.height / 4;
	//	spinButtonDifference = new Vector3(x, y, 0);

	//	float distToWheel = Mathf.Abs (spinButton.position.x - wheel.position.x) * 1.55f;
	//	Vector3 posForAdButton = Vector3.zero;
	//	posForAdButton.x -= distToWheel;

		//spinButton.localPosition = posForSpinButton;
		//spinButton.GetChild (2).localPosition = new Vector2 ( - (Screen.width/1.65f), 0);
		//x = spinButton.transform.position.x - Screen.width + (spinButton.GetChild(2).GetComponent<Image>().rectTransform.rect.width*1.2f);
		//y = spinButton.transform.position.y;
		//spinButton.GetChild (2).position = new Vector2 (x,y);

		//spinButton.GetChild (2).gameObject.SetActive (false);

		wheelRot = UnityEngine.Random.Range (0,360);
		rot.z = wheelRot;
		wheel.rotation = Quaternion.Euler (rot);


	}

	IEnumerator SetCanPlay() {
		// just a bit of delay, it looks nicer to wait before coming in
		yield return new WaitForSeconds (2);
		canPlayToday = true;
	}
	
	// Update is called once per frame
	void Update () {

		if (canPlayToday) {
			FlashButton ();
		}

		if (!done) {
			if (isSpinning || hasStopped) {

				if (isSpinning) {
					if (speed < wheelSpeed) {
						speed += Time.deltaTime * (wheelSpeed / 3);
					} else {
						isSpinning = false;
						hasStopped = true;
					}
				} else {
					if (speed > 15) {
						if (speed > 70) {
							speed -= Time.deltaTime * (wheelSpeed / 10);
						} else if (speed > 50) {
							speed -= Time.deltaTime * (wheelSpeed / 7);
						} else if (speed > 35) {
							speed -= Time.deltaTime * (wheelSpeed / 5);
						} else {
							speed -= Time.deltaTime * (wheelSpeed/3);
						}
					} else {
						speed -= Time.deltaTime * 3;
						StartCoroutine ("FinalStop");
					}
				}

				wheelRot += Time.deltaTime * speed;
				rot.z = wheelRot;
				wheel.rotation = Quaternion.Euler (rot);
			}
		}

		float buttonStep = 450 * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, buttonMoveTarget, buttonStep);

		if (slideInWheel) {
			float step = 2000 * Time.deltaTime;
			Vector3 targetPos = new Vector3 (Screen.width / 2, Screen.height / 2, 0);
			wheelParent.position = Vector3.MoveTowards (wheelParent.position, targetPos, step);
			//spinButton.position = wheelParent.position + spinButtonDifference;
			//spinButton.GetChild (2).position = new Vector2(wheelParent.position.x - spinButtonDifference.x, spinButton.position.y);
		}

		if (pointer.transform.GetChild(0).eulerAngles.z > 45 && pointer.transform.GetChild(0).eulerAngles.z < 200) {
			pointer.transform.GetChild(0).rotation = Quaternion.Euler (new Vector3(0,0,45));
		}
	}

	void FlashButton() {
		if (!openButtonFadeDir) {
			if (openButtonColour.r > 0.5f) {
				openButtonColour.r -= Time.deltaTime;
				openButtonColour.g = openButtonColour.b = openButtonColour.r;
				openButtonImage.color = openButtonColour;
			} else {
				openButtonFadeDir = true;
			}
		} else {
			if (openButtonColour.r < 1) {
				openButtonColour.r += Time.deltaTime;
				openButtonColour.g = openButtonColour.b = openButtonColour.r;
				openButtonImage.color = openButtonColour;
			} else {
				openButtonFadeDir = false;
			}
		}
	}

	public GameObject usedBonusSpinMessage;

	public void BringInWheelAfterAd() {
		slideInWheel = true;
		spinWheelButton.interactable = true;
		noSpinsWarning.SetActive (false);
	}

	public void BringInWheel() {
		//if (wheel.transform.position.x > transform.position.x) {
		//	gameMenu.SetActive (false);
		//	spinButton.GetChild (2).gameObject.SetActive (true);
			slideInWheel = true;
			if (!canPlayToday ) {
				spinWheelButton.interactable = false;
				noSpinsWarning.SetActive (true);
			} else {
				

				spinWheelButton.interactable = true;
				noSpinsWarning.SetActive (false);
			}
		//} else if (!isSpinning && !hasStopped) {
		//	gameMenu.SetActive (true);
		//	Recycle ();

		//}
	}

	public void WatchAdForSpin() {
#if UNITY_ANDROID || UNITY_IOS
        GameObject.Find ("InAppPurchasingController").GetComponent<IAppPurchaser>().WatchAdForPar();
#endif
	}

	public void Recycle() {

		GameObject g = (GameObject)Instantiate (wheelPrefab,  transform.parent.position - startPos, Quaternion.identity);
		g.transform.position = new Vector3 (1042,107,0);
		g.transform.SetParent (transform.parent, true);
		g.GetComponent<SpinWheel> ().gameMenu = gameMenu;
		g.GetComponent<SpinWheel> ().wheelPrefab = wheelPrefab;
		g.GetComponent<SpinWheel> ().resultsMenuObject = resultsMenuObject;
		g.transform.localScale = transform.localScale;
		g.name = name;
		Destroy (gameObject);
	}

	public void StartSpin(Button thisButton) {
		thisButton.interactable = false;
		isSpinning = true;
	}

	IEnumerator FinalStop() {
		yield return new WaitForSeconds (4);
		if (!done) {
			speed = 0;
			done = true;
			FindResult ();
		}
	}

	void FindResult() {
		float d = 999999999f;
		SpinWheelPoint closestPoint = null;
		foreach (SpinWheelPoint g in points) {
			float dist = Vector3.Distance (g.transform.position, pointerPoint.transform.position);
			if (dist < d) {
				d = dist;
				closestPoint = g;
			}
		}


		Transform resultsMenu = resultsMenuObject.transform;
		RawImage rewardImage = null; Text rewardText = null;
		for (int i = 0; i < resultsMenu.childCount; i++) {
			if (resultsMenu.GetChild (i).name == "RewardImage") {
				rewardImage = resultsMenu.GetChild (i).GetComponent<RawImage> () as RawImage;
			} else if (resultsMenu.GetChild(i).name == "RewardText") {
				rewardText = resultsMenu.GetChild (i).GetComponent<Text> () as Text;
			}
		}

		switch (closestPoint.type) {
		case (SpinWheelPoint.RewardType.Par1):
			rewardImage.texture = rewardTextures [0];
			rewardText.text = "+1 Par!";
			AddPar (1);
			break;
		case (SpinWheelPoint.RewardType.Par2):
			rewardImage.texture = rewardTextures [0];
			rewardText.text = "+2 Par!";
			AddPar (2);
			break;
		case (SpinWheelPoint.RewardType.Eraser1):
			rewardImage.texture = rewardTextures [1];
			rewardText.text = "+1 Eraser!";
			AddEraser (1);
			break;
		case (SpinWheelPoint.RewardType.Eraser2):
			rewardImage.texture = rewardTextures [1];
			rewardText.text = "+3 Erasers!";
			AddEraser (3);
			break;
		case (SpinWheelPoint.RewardType.SkipHole1):
			rewardImage.texture = rewardTextures [2];
			rewardText.text = "+1 Hole Skip!";
			AddHoleSkips (1);
			break;
		case (SpinWheelPoint.RewardType.UnlockLevel):
			rewardImage.texture = rewardTextures [3];
			rewardText.text = "+1 Level Unlock!";

			int num = 1;
			if (PlayerPrefs.HasKey ("LevelUnlocker")) {
				num += PlayerPrefs.GetInt ("LevelUnlocker");
			}

			PlayerPrefs.SetInt ("LevelUnlocker", num);

			break;
		}
		resultsMenuObject.SetActive(true);
	}



	void AddPar(int amount) {
		

		int num = 0;
		if (PlayerPrefs.HasKey ("sparepar")) {
			num += PlayerPrefs.GetInt ("sparepar");
		}

		num += amount;

		PlayerPrefs.SetInt ("sparepar", num);
		IncrementSpinCount ();
	}

	void AddEraser(int amount) {
		
		int num = 0;
		if (PlayerPrefs.HasKey ("Erasers")) {
			num += PlayerPrefs.GetInt ("Erasers");
		}
		num += amount;

		PlayerPrefs.SetInt ("Erasers", num);
		IncrementSpinCount ();
	}

	void AddHoleSkips(int amount) {
		
		int num = 0;
		if (PlayerPrefs.HasKey ("HoleSkip")) {
			num += PlayerPrefs.GetInt ("HoleSkip");
		}
		num += amount;

		PlayerPrefs.SetInt ("HoleSkip", num);
		IncrementSpinCount ();
	}

	void IncrementSpinCount() {

		int numSpins = PlayerPrefs.GetInt ("NumSpins");
		currentDate = System.DateTime.Now;

		numSpins++;
		PlayerPrefs.SetInt ("NumSpins", numSpins);

		PlayerPrefs.SetString ("LastTimeSpunWheel", currentDate.ToString());


	}

	IEnumerator ShowUsedBonusSpin() {

		yield return new WaitForSeconds (0);

		Vector2 pos = new Vector2 (Screen.width/2, Screen.height/2);
		GameObject g = (GameObject)Instantiate (usedBonusSpinMessage, pos, Quaternion.identity);
		float h = g.GetComponent<Image> ().rectTransform.rect.height;
		float w = 600;
		g.GetComponent<Image> ().rectTransform.sizeDelta = new Vector2 (w, h);
		g.transform.GetChild (0).GetComponent<Text> ().text = "Used Bonus Spin! (" + PlayerPrefs.GetInt("ExtraSpins") + ")";
		g.transform.SetParent (GameObject.Find("Spin Results").transform);
	}
}
