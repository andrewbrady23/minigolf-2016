﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

#if UNITY_ANDROID || UNITY_IOS
using UnityEngine.Purchasing;
using Heyzap;

public class IAppPurchaser : MonoBehaviour, IStoreListener {

	private static IStoreController m_StoreController;                                                                  // Reference to the Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider;                                                         // Reference to store-specific Purchasing subsystems.

	public static GameObject rewardResultPrefab;


	// Ad Watch Stuff
//	public delegate void AdDisplayListener(string state, string tag);
//	private static AdDisplayListener adDisplayListener;



	// Pulled From Global Variables

	public static int player_cannon_coins;


	public static string iapFirstPaymentName 	= "cd_coins_400u";
	public static string iapSecondPaymentName 	= "cd_coins_1000u";
	public static string iapThirdPaymentName	= "cd_coins_3000u";
	public static string iapFourthPaymentName	= "cd_coins_10000u";
	public static string iapFifthPaymentName	= "cd_coins_30000u";
	public static string iapSixthPaymentName	= "cd_coins_100000u"; 

	public static string iapFirstPaymentNameGoogle 	= "cd_coins_400m";
	public static string iapSecondPaymentNameGoogle = "cd_coins_1000m";
	public static string iapThirdPaymentNameGoogle	= "cd_coins_3000m";
	public static string iapFourthPaymentNameGoogle	= "cd_coins_10000m";
	public static string iapFifthPaymentNameGoogle	= "cd_coins_30000m";
	public static string iapSixthPaymentNameGoogle	= "cd_coins_100000m"; 


	public static int iapFirstPayAmount 	= 400;
	public static int iapSecondPayAmount  	= 1000;
	public static int iapThirdPayAmount 	= 3000;
	public static int iapFourthPayAmount 	= 10000;
	public static int iapFifthPayAmount 	= 30000;
	public static int iapSixthPayAmount 	= 100000;

	public GameObject cantUnlockLevel;

	public GameObject restorePurchasesButton;

	public GameObject adSpinnerButton;
	// Use this for initialization
	void Start () {

		if (!Main.hasPurchaser) {
			Main.hasPurchaser = true;
			DontDestroyOnLoad (this);
		} else {
			Destroy (gameObject);
		}


#if UNITY_IOS

#else
		restorePurchasesButton.SetActive(false);
#endif

		HZIncentivizedAd.Fetch ();
		HZIncentivizedAd.SetDisplayListener (listener);

		//CheckforIncentAd ();
		
		// If we haven't set up the Unity Purchasing reference
		if (m_StoreController == null) {
			// Begin to configure our connection to Purchasing
			InitializePurchasing ();
		} else {
			//FindPrices ();
		}

		//listedPrices [13].text = "+" + Main.parsPerAd + " shots to watch an ad!";
	}
	
	// Update is called once per frame
	void Update () {
		if (rewardDisplay != null) {

			RawImage rewardImage = rewardDisplay.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<RawImage> (); 

			switch (rewarded) {
			case "erasers":
				rewardImage.texture = rewardIcons [0];
				break;
			case "skiphole":
				rewardImage.texture = rewardIcons [1];
				break;
			case "pars":
				rewardImage.texture = rewardIcons [2];
				break;
			}

			rewardDisplay = null;
			rewarded = "";
		}

		if (watchedAd) {
			PostAdSpin ();
		}
	}


	public GameObject spinner;
	public void HideSpinner() {
		spinner.SetActive (false);
	}

	public void ShowSpinner() {
		spinner.SetActive (true);
	}

	public void PostAdSpin() {
		GameObject.Find ("Canvas/MainPanel/Store").SetActive(false);
		spinner.SetActive (true);
		spinner.GetComponent<SpinWheel>().BringInWheelAfterAd();
		musicPlayer.Unmute ();
		watchedAd = false;
	}


	static bool watchedAd = false;
	static string rewarded = "";
	HZIncentivizedAd.AdDisplayListener listener = delegate(string adState, string adTag){
		if (adState.Equals ("incentivized_result_complete")) {
			Debug.Log("Watched Ad");

			watchedAd = true;


			//spinner.GetComponent<SpinWheel>().canPlayToday = true;
			//spinner.GetComponent<SpinWheel>().BringInWheel();


			/*
			int extraSpins = 1;

			if (PlayerPrefs.HasKey("ExtraSpins")) {
				extraSpins += PlayerPrefs.GetInt("ExtraSpins");
			}

			PlayerPrefs.SetInt("ExtraSpins", extraSpins);
		*/

		}
		if ( adState.Equals("incentivized_result_incomplete") ) {
			Debug.Log("incomplete ad");

			ShowIncentivisedAdResult("Didn't View Ad!");
		}

		if ( adState.Equals("fetch_failed") ) {
			Debug.Log("failed to fetch ad");
			//ShowIncentivisedAdResult("Didn't Fetch Ad!");
		}
	};


	public Texture[] rewardIcons;
	static GameObject rewardDisplay;
	static void ShowIncentivisedAdResult(string message) {
		GameObject g = (GameObject)Instantiate(Resources.Load("AdRewardResult"), Vector3.zero, Quaternion.identity) as GameObject;
		g.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = message;
		if (message == "Didn't View Ad!" || message == "Didn't Fetch Ad!") {
			Destroy(g.transform.GetChild (0).GetChild (0).GetChild (0).gameObject);
		}

		rewardDisplay = g;
	}




	public Text[] listedPrices;
	void FindPrices() {
		Product[] products = m_StoreController.products.all;
		for (int i = 0; i < products.Length; i++) {
			string name = products [i].definition.storeSpecificId;
			string price = products [i].metadata.localizedPriceString;
			switch (name) {

			case "com.onteca.removeads":
				listedPrices [0].text = price;
				Main.removeAdsCost = price;
				break;
			case "com.onteca.extralevel":
				listedPrices [1].text = price;
				listedPrices [12].text = price;
				Main.extraLevelsCost = price;
				break;
			case "com.onteca.parup10":
				listedPrices [2].text = price;
				Main.par10Cost = price;
				break;
			case "com.onteca.parup25":
				listedPrices [3].text = price;
				Main.par25Cost = price;
				break;
			case "com.onteca.parup60":
				listedPrices [4].text = price;
				Main.par60Cost = price;
				break;
			case "com.onteca.parup150":
				listedPrices [5].text = price;
				Main.par150Cost = price;
				break;
			case "com.onteca.eraser5":
				listedPrices [6].text = price;
				Main.eraser5Cost = price;
				break;
			case "com.onteca.eraser10":
				listedPrices [7].text = price;
				Main.eraser10Cost = price;
				break;
			case "com.onteca.eraser15":
				listedPrices [8].text = price;
				Main.eraser15Cost = price;
				break;
			case "com.onteca.skiphole2":
				listedPrices [9].text = price;
				Main.skipHole2Cost = price;
				break;
			case "com.onteca.skiphole5":
				listedPrices [10].text = price;
				Main.skipHole5Cost = price;
				break;
			case "com.onteca.skiphole7":
				listedPrices [11].text = price;
				Main.skipHole7Cost = price;
				break;
			}
//			Debug.Log(products[i].definition.storeSpecificId);
		}
	}

	public MusicPlayer musicPlayer;
	public void WatchAdForPar() {

		if (HZIncentivizedAd.IsAvailable ()) {
			Debug.Log ("Showing Ad...");
			HZIncentivizedAd.Show ();
			musicPlayer.Mute ();
		} else {
			Debug.Log ("Couldn't Find Ad");
			ShowIncentivisedAdResult("Didn't Fetch Ad!");
		}

		//ShowIncentivisedAd();
		//Debug.Log("Loading Ad...");

	}

	public void CheckForIncentAd() {
		if (HZIncentivizedAd.IsAvailable ()) {
			adSpinnerButton.SetActive (true);
		} else {
			adSpinnerButton.SetActive (false);
		}
	}

	public void ShowIncentivisedAd() {
		Debug.Log ("ShowIncentivised called");
		if (HZIncentivizedAd.IsAvailable ()) {
			Debug.Log ("ShowIncentivised avail/show");
			HZIncentivizedAd.Show ();
		} else {
			Debug.Log ("Incent Fetch failed try again from button press");
			HZIncentivizedAd.Fetch ();
		}
	}

	void CheckforIncentAd() {
		if (HZIncentivizedAd.IsAvailable ()) {
			Debug.Log ("Ad is ready");
			//pulseAnim.Pulse();
		} else {
			//pulseAnim.StopPulse();
			Debug.Log("Pulling Ad...");
			HZIncentivizedAd.Fetch();
		}
	}



	public void BuyRemoveAds() {
		BuyNonConsumable ("removeads");
	}
	public void BuyLevels() {
		BuyNonConsumable ("extralevels");
	}
	public void BuyPar(int amount) {
		BuyNonConsumable ("parup", amount);
	}
	public void BuyEraser(int amount) {
		BuyNonConsumable ("eraser", amount);
	}
	public void BuyHoleSkips(int amount) {
		BuyNonConsumable ("skiphole", amount);
	}

	MapLoader passOn;
	public void SetSelectedLevelToUnlock(MapLoader map) {
		Main.selectedLevel = map.hole;
		passOn = map;
	}
	public void UnlockLevel () {
		if (PlayerPrefs.HasKey ("LevelUnlocker") && PlayerPrefs.GetInt ("LevelUnlocker") > 0) {
			PlayerPrefs.SetInt ("UnlockedLevel" + Main.selectedLevel, 1);
			int num = PlayerPrefs.GetInt ("LevelUnlocker");
			num--;
			PlayerPrefs.SetInt ("LevelUnlocker", num);


			for (int i = 0; i < passOn.transform.childCount; i++) {
				if (passOn.transform.GetChild (i).name == "Padlock") {
					if (PlayerPrefs.HasKey ("UnlockedLevel" + Main.selectedLevel)) {
						passOn.transform.GetChild (i).gameObject.SetActive (false);
					}
				}
			}


		} else {
			Instantiate (cantUnlockLevel, Vector2.zero, Quaternion.identity);
		}
	}


	public void BuyNonConsumable(string item, int amount = default(int))
	{
		switch (item) {
		case "removeads":
			BuyProductID( "com.onteca.removeads" );
			break;

		case "extralevels":
			BuyProductID( "com.onteca.extralevel" );
			break;

		case "parup":
			if (amount > 0) {
				BuyProductID ("com.onteca.parup" + amount);
			} else {
				Debug.LogError ("No Amount of Pars Specified!");
			}
			break;

		case "eraser":
			if (amount > 0) {
				BuyProductID ("com.onteca.eraser" + amount);
			} else {
				Debug.LogError ("No Amount of Erasers Specified!");
			}
			break;
		case "skiphole":
			if (amount > 0) {
				BuyProductID ("com.onteca.skiphole" + amount);
			} else {
				Debug.LogError ("No Amount of Hole Skips Specified!");
			}
			break;
		}





		// Buy the non-consumable product using its general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
		//BuyProductID(kProductIDNonConsumable);
	}


	void BuyProductID(string productId)
	{
		// If the stores throw an unexpected exception, use try..catch to protect my logic here.
		try
		{
			// If Purchasing has been initialized ...
			if (IsInitialized())
			{
				// ... look up the Product reference with the general product identifier and the Purchasing system's products collection.
				Product product = m_StoreController.products.WithID(productId);

				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase)
				{
//					Debug.Log (string.Format("Purchasing product asychronously: '{0}'", product.definition.id));// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
					m_StoreController.InitiatePurchase(product);
				}
				// Otherwise ...
				else
				{
					// ... report the product look-up failure situation  
					Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			}
			// Otherwise ...
			else
			{
				// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or retrying initiailization.
				Debug.Log("BuyProductID FAIL. Not initialized.");
			}
		}
		// Complete the unexpected exception handling ...
		catch (Exception e)
		{
			// ... by reporting any unexpected exception for later diagnosis.
			Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
		}
	}

	public void InitializePurchasing() 
	{
		// If we have already connected to Purchasing ...
		if (IsInitialized())
		{
			// ... we are done here.
			return;
		}

		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());


		builder.Configure<IGooglePlayConfiguration>().
		SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhh+oy8ppEM7waC9yTk33y6nX1SAJ9Acl9y1C58xuRPGuEtGxGJlSnrE67gB2hbXJwSBerOgkA/IhGmcqrAuXhDHXTpjp7E2CP++H5uWCDt5Ao2NGOepcOJ/HWwJg3y4eLwE26FSmSc6TRCINENnRptACSKE7G0GiiKxJjRGsm45QbcoRi2hBUaS/O3Yp696NYQSPBlknxpWO7KzLqG/jjyAun+hQHkTSIpAqD4EZ/9ehZ7oWOSQpCRh0LldW2MXLtmPQpmmTTtTnkE8B+eFYFXXgrTIrbYvXFYk/0vD9F84m610TIgPcXA5aWsSXT9XLeJH4lafsTHB97z7mFm8ezQIDAQAB");


		builder.AddProduct ("com.onteca.removeads", ProductType.NonConsumable,
			new IDs () {
				{ "com.onteca.removeads", GooglePlay.Name}
			}
		);

		builder.AddProduct ("com.onteca.extralevel", ProductType.NonConsumable,
			new IDs () {
				{ "com.onteca.extralevel", GooglePlay.Name}
			}
		);

		builder.AddProduct ("com.onteca.parup", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.parup", GooglePlay.Name}
			}
		);

		builder.AddProduct ("com.onteca.parup10", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.parup10", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.parup25", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.parup25", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.parup60", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.parup60", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.parup150", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.parup150", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.eraser5", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.eraser5", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.eraser10", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.eraser10", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.eraser15", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.eraser15", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.skiphole2", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.skiphole2", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.skiphole5", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.skiphole5", GooglePlay.Name}
			}
		);
		builder.AddProduct ("com.onteca.skiphole7", ProductType.Consumable,
			new IDs () {
				{ "com.onteca.skiphole7", GooglePlay.Name}
			}
		);


		UnityPurchasing.Initialize(this, builder);
	}


	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log("OnInitialized: PASS");

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;

		// should have all the prices by now so try it
		FindPrices ();
	}


	public void OnInitializeFailed(InitializationFailureReason error)
	{
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log("Billing failed to initialize!");
		switch (error)
		{
		case InitializationFailureReason.AppNotKnown:
			Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
			break;
		case InitializationFailureReason.PurchasingUnavailable:
			// Ask the user if billing is disabled in device settings.
			Debug.Log("Billing disabled!");
			break;
		case InitializationFailureReason.NoProductsAvailable:
			// Developer configuration error; check product metadata.
			Debug.Log("No products available for purchase!");
			break;
		}
	}


	public GameObject messagePrefab;
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
	{
		//Debug.Log (" Purchased: " + args.purchasedProduct.definition.id + ":" + args.purchasedProduct.definition.type);
		GameObject tempgo;
		int player_coins = 0;

		string boughtMessage = "";

		if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.removeads", StringComparison.Ordinal)) {
			Debug.Log ("remove ads purchase");
			PlayerPrefs.SetInt("removeads", 1);
			tempgo = GameObject.Find ("removeadstext") as GameObject;
			if (tempgo != null) { 
				tempgo.GetComponent<TextMesh> ().text = "PURCHASED";
			}
			Main.theAdController.HideBanner ();
			boughtMessage = "Ads Removed!";
		} else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.extralevel", StringComparison.Ordinal)) {
			Debug.Log ("extra levels purchase");
			PlayerPrefs.SetInt("extralevels", 1);
			tempgo = GameObject.Find ("unlockextralevelstext") as GameObject;
			if (tempgo != null) { 
				tempgo.GetComponent<TextMesh> ().text = "PURCHASED";
			}
			boughtMessage = "Levels Unlocked!";
		} 
		//consumables
		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.parup", StringComparison.Ordinal)) {
			GameScript g = GameObject.Find ("gameScriptReg(Clone)").GetComponent<GameScript>() as GameScript;
			g.ParUp (1);
			g.ContinueOverPar ();
		}


		// Par Purchasing
		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.parup10", StringComparison.Ordinal)) {
			if (Application.loadedLevel > 0) {
				GameScript g = GameObject.Find ("gameScriptReg(Clone)").GetComponent<GameScript> () as GameScript;
				g.ParUp (10);
				g.ContinueOverPar ();
			} else {
				int pars = 10;
				if (PlayerPrefs.HasKey ("sparepar")) {
					pars += PlayerPrefs.GetInt ("sparepar");
				}

				PlayerPrefs.SetInt ("sparepar", pars);
				boughtMessage = "+10 Par!";
			}
		}

		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.parup25", StringComparison.Ordinal)) {
			if (Application.loadedLevel > 0) {
				GameScript g = GameObject.Find ("gameScriptReg(Clone)").GetComponent<GameScript> () as GameScript;
				g.ParUp (25);
				g.ContinueOverPar ();
			} else {
				int pars = 25;
				if (PlayerPrefs.HasKey ("sparepar")) {
					pars += PlayerPrefs.GetInt ("sparepar");
				}

				PlayerPrefs.SetInt ("sparepar", pars);
				boughtMessage = "+25 Par!";
			}
		}

		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.parup60", StringComparison.Ordinal)) {
			if (Application.loadedLevel > 0) {
				GameScript g = GameObject.Find ("gameScriptReg(Clone)").GetComponent<GameScript> () as GameScript;
				g.ParUp (60);
				g.ContinueOverPar ();
			} else {
				int pars = 60;
				if (PlayerPrefs.HasKey ("sparepar")) {
					pars += PlayerPrefs.GetInt ("sparepar");
				}

				PlayerPrefs.SetInt ("sparepar", pars);
				boughtMessage = "+60 Par!";
			}
		}

		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.parup150", StringComparison.Ordinal)) {
			if (Application.loadedLevel > 0) {
				GameScript g = GameObject.Find ("gameScriptReg(Clone)").GetComponent<GameScript> () as GameScript;
				g.ParUp (150);
				g.ContinueOverPar ();
			} else {
				int pars = 150;
				if (PlayerPrefs.HasKey ("sparepar")) {
					pars += PlayerPrefs.GetInt ("sparepar");
				}

				PlayerPrefs.SetInt ("sparepar", pars);
				boughtMessage = "+150 Par!";
			}
		}

		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.eraser5", StringComparison.Ordinal)) {
			int erasers = 5;
			if (PlayerPrefs.HasKey ("Erasers")) {
				erasers += PlayerPrefs.GetInt ("Erasers");
			}

			PlayerPrefs.SetInt ("Erasers", erasers);
			boughtMessage = "+5 Erasers!";
		}

		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.eraser10", StringComparison.Ordinal)) {
			int erasers = 10;
			if (PlayerPrefs.HasKey ("Erasers")) {
				erasers += PlayerPrefs.GetInt ("Erasers");
			}

			PlayerPrefs.SetInt ("Erasers", erasers);
			boughtMessage = "+10 Erasers!";
		}

		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.eraser15", StringComparison.Ordinal)) {
			int erasers = 15;
			if (PlayerPrefs.HasKey ("Erasers")) {
				erasers += PlayerPrefs.GetInt ("Erasers");
			}

			PlayerPrefs.SetInt ("Erasers", erasers);
			boughtMessage = "+15 Erasers!";
		}

		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.skiphole2", StringComparison.Ordinal)) {
			int holeskips = 2;
			if (PlayerPrefs.HasKey ("HoleSkip")) {
				holeskips += PlayerPrefs.GetInt ("HoleSkip");
			}

			PlayerPrefs.SetInt ("HoleSkip", holeskips);
			boughtMessage = "+2 Hole Skips!";
		}
		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.skiphole5", StringComparison.Ordinal)) {
			int holeskips = 5;
			if (PlayerPrefs.HasKey ("HoleSkip")) {
				holeskips += PlayerPrefs.GetInt ("HoleSkip");
			}

			PlayerPrefs.SetInt ("HoleSkip", holeskips);
			boughtMessage = "+5 Hole Skips!";
		}
		else if (String.Equals (args.purchasedProduct.definition.id, "com.onteca.skiphole7", StringComparison.Ordinal)) {
			int holeskips = 7;
			if (PlayerPrefs.HasKey ("HoleSkip")) {
				holeskips += PlayerPrefs.GetInt ("HoleSkip");
			}

			PlayerPrefs.SetInt ("HoleSkip", holeskips);
			boughtMessage = "+7 Hole Skips!";
		}



		// Failed
		else 
		{
			Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
			return PurchaseProcessingResult.Complete;
		}


		if (Application.loadedLevel == 0) {
		Vector2 pos = new Vector2 (Screen.width/2, Screen.height/2);

		GameObject m = (GameObject)Instantiate(messagePrefab, pos, Quaternion.identity);
		Text t = m.transform.GetChild (0).GetComponent<Text> () as Text;
		t.text = boughtMessage;

		if (boughtMessage == "Levels Unlocked!") {
			t.fontSize = 55;
		}

	//	if (Application.loadedLevel == 0) {
			m.transform.parent = GameObject.Find ("MainMenu/Canvas").transform;
	//	} else {
	//		m.transform.parent = GameObject.Find ("gameScriptReg(Clone)/Canvas").transform;
		//}
		}



		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing this reason with the user.
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",product.definition.storeSpecificId, failureReason));
	}

	private bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}



	public void CloseResultsMenu() {

		SpinWheel sw = GameObject.Find ("Canvas/MainPanel/SpinnerButton").GetComponent<SpinWheel>() as SpinWheel;

		//sw.gameMenu.SetActive (true);
		//sw.resultsMenuObject.SetActive (false);

		sw.Recycle ();
	}


	public void RestorePurchases()
	{
		// If Purchasing has not yet been set up ...
		if (!IsInitialized())
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}

	/*

	void OnGUI() {
		if (GUI.Button (new Rect (100, 100, 100, 100), "DEL")) {
			PlayerPrefs.DeleteAll ();
		}
	}

*/
}
#endif
