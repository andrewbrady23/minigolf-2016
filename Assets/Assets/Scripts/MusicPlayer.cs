﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	public float timeBetweenSongs;

	public AudioClip[] songs;
	int index = 0;

	AudioSource audio;

	public Text musicButton;

	bool play;
	bool startCountDown;
	float count;

	// Use this for initialization
	void Start () {

		if (!Main.musicPlayerExists) {
			audio = GetComponent<AudioSource> ();
			audio.clip = songs [0];
			audio.Play ();

			DontDestroyOnLoad (gameObject);

			if (PlayerPrefs.HasKey ("MusicEnabled")) {
				int i = PlayerPrefs.GetInt ("MusicEnabled");
				if (i == 0) {
					audio.volume = 0;
					play = false;
					musicButton.text = "Music: Off";
				} else {
					audio.volume = 1;
					play = true;
					musicButton.text = "Music: On";
				}
			} else {
				play = true;
			}
			Main.musicPlayerExists = true;
		} else {
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (play) {
			if (!audio.isPlaying) {
				if (!startCountDown) {
					startCountDown = true;
				} else {
					count += Time.deltaTime;
				}
				if (count >= timeBetweenSongs) {
					if (index < songs.Length) {
						index++;
					} else {
						index = 0;
					}
					audio.volume = 0;
					audio.clip = songs [index];
					audio.Play ();
					startCountDown = false;
					count = 0;
				}
			} else if (play) {
				if (audio.volume < 1) {
					audio.volume += Time.deltaTime/3;
				}
			}
		}
	}

	public void MusicEnabled() {
		if (!play) {
			audio.volume = 1;
			PlayerPrefs.SetInt ("MusicEnabled", 1);
			musicButton.text = "Music: On";
		} else {
			audio.volume = 0;
			PlayerPrefs.SetInt ("MusicEnabled", 0);
			musicButton.text = "Music: Off";
		}
		play = !play;
	}

	public void Mute() {
		play = false;
		audio.volume = 0;
	}
	public void Unmute() {
		play = true;
		audio.volume = 1;
	}
}
