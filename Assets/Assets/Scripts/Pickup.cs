﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

	public PickupType type;
	public enum PickupType {
		ShotDown
	}

	public float rotSpeed;
	public Transform glow;
	Vector3 glowScale = Vector3.one * 1.3f;

	float glowYRot=0;
	float scaleSpeed = 1;

	bool xScale=false;
	bool yScale=false;
	bool zScale=false;

	void Update() {
		if (glow != null) {

			if (glow.transform.localScale.x < 1.2f) {
				if (!xScale) {
					xScale = true;
				}
			} else if (glow.transform.localScale.x > 1.6f) {
				if (xScale) {
					xScale = false;
				}
			}
			if (!xScale) {
				glowScale.x -= Random.Range(0f,1f) * Time.deltaTime * scaleSpeed;
			} else {
				glowScale.x += Random.Range(0f,1f) * Time.deltaTime * scaleSpeed;
			}

			if (glow.transform.localScale.y < 1.2f) {
				if (!yScale) {
					yScale = true;
				}
			} else if (glow.transform.localScale.y > 1.6f) {
				if (yScale) {
					yScale = false;
				}
			}
			if (!yScale) {
				glowScale.y -= Random.Range(0f,1f) * Time.deltaTime * scaleSpeed;
			} else {
				glowScale.y += Random.Range(0f,1f) * Time.deltaTime * scaleSpeed;
			}


			if (glow.transform.localScale.z < 1.2f) {
				if (!zScale) {
					zScale = true;
				}
			} else if (glow.transform.localScale.z > 1.6f) {
				if (zScale) {
					zScale = false;
				}
			}
			if (!zScale) {
				glowScale.z -= Random.Range(0f,1f) * Time.deltaTime * scaleSpeed;
			} else {
				glowScale.z += Random.Range(0f,1f) * Time.deltaTime * scaleSpeed;
			}



			glow.transform.localScale = glowScale;
			glowYRot += Time.deltaTime * rotSpeed;
			glow.transform.rotation = Quaternion.Euler (0,glowYRot, 0);




			/*
			int minX = glow.transform.localScale.x > 1.6f ? -1 : 0;
			int maxX = glow.transform.localScale.x < 2f ? 1 : 0;

			Debug.Log ("min x: " + minX + ", max x: " + maxX);

			float minY = glow.transform.localScale.y > 1f ? -1 : 0;
			float maxY = glow.transform.localScale.y < 2f ? 1 : 0;

			float minZ = glow.transform.localScale.z > 1f ? -1 : 0;
			float maxZ = glow.transform.localScale.z < 2f ? 1 : 0;

			glowScale.x += Random.Range (minX, maxX) * Time.deltaTime;
			glowScale.y += Random.Range (minY, maxY) * Time.deltaTime;
			glowScale.z += Random.Range (minZ, maxZ) * Time.deltaTime;

			glow.transform.localScale = glowScale;

			
		*/
		}
	}

	void ExplodeOuter() {

	}
}
