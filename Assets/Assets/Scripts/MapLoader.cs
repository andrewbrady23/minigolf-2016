﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapLoader : MonoBehaviour {

	public int hole;
	RawImage buttonImg;
	Button button;
	Text title;
	Text score;

	public GameObject padlock;
	public RawImage border;
	bool thisHoleSelected = false;

	float a = 0;

	public GameObject[] stars;
	Image[] starRenderers;

	public Image thumbnail;
	//public RawImage border;

//	public GameObject playedBeforeIcon;
	public ScrollRect scrollRect;

	public bool isLocked {
		set {
			button.interactable = !value;

			if (value) {
				stars [0].transform.parent.gameObject.SetActive (false);
			} else {
				border.color = new Color (0, 0.7f, 0);
			}

			if (hole > 36)
			for (int i = 0; i < transform.childCount; i++) {
				if (transform.GetChild (i).name == "Padlock") {
					if (PlayerPrefs.HasKey ("UnlockedLevel" + hole)) {
						transform.GetChild (i).gameObject.SetActive (false);
					}
				}
			}
		}
	}



	// Use this for initialization
	void Awake () {

		//scrollRect = transform.parent.parent.parent.parent.parent.GetComponent<ScrollRect> ();

		button = GetComponent<Button> () as Button;

		//buttonImg = GetComponent<RawImage> () as RawImage;
		title = transform.GetChild (3).GetComponent<Text>() as Text;
		score = title.transform.GetChild (0).GetComponent <Text>() as Text;

		//Color c1 = buttonImg.color;
		Color c2 = title.color;
		Color c3 = score.color;
		//c1.a = 
		c2.a = c3.a = a;

		//buttonImg.color = c1;
		title.color = c2;
		score.color = c3;

		title.text = ("Hole " + hole.ToString());

		string thisKey = "Level"+hole.ToString()+"Score";
		if (PlayerPrefs.HasKey (thisKey) && PlayerPrefs.GetInt (thisKey) != 1000) {
//			Debug.Log ("Setting score for map " + hole.ToString());
			string text = "High Score: " + PlayerPrefs.GetInt (thisKey);
			score.text = text;
		} else {
			score.text = ("High Score: " + "N/A");
		}

		if (PlayerPrefs.HasKey (thisKey)) {
			if (PlayerPrefs.GetInt (thisKey) <= 1) {
				for (int i = 0; i < stars.Length; i++) {
					stars [i].transform.GetChild (0).gameObject.SetActive (false);
					stars [i].transform.GetChild (1).gameObject.SetActive (true);
				}
			}
			if (PlayerPrefs.GetInt (thisKey) == 2) {
				for (int i = 0; i < stars.Length - 1; i++) {
					stars [i].transform.GetChild (0).gameObject.SetActive (false);
					stars [i].transform.GetChild (1).gameObject.SetActive (true);
				}
			}
			if (PlayerPrefs.GetInt (thisKey) == 3) {
				for (int i = 0; i < stars.Length - 2; i++) {
					stars [i].transform.GetChild (0).gameObject.SetActive (false);
					stars [i].transform.GetChild (1).gameObject.SetActive (true);
				}
			}
		}


		bool isLocked_;
		if (PlayerPrefs.HasKey ("UnlockedLevels")) {

			char[] charArray = PlayerPrefs.GetString ("UnlockedLevels").ToCharArray ();

			if (charArray.Length >= hole - 1 || hole == 1) {
				isLocked_ = false;
				//if (charArray.Length >= hole - 1 && charArray.Length >= hole) {
				//	playedBeforeIcon.SetActive (true);
				//}
			} else {
				isLocked_ = true; 
			}

			if (charArray.Length == hole-1) {
				thisHoleSelected = true;
				stars [0].transform.parent.gameObject.SetActive (false);

				// move to highlight current hole need to get to nearest 5 above
				int rounded = (hole + 5)/5-1;
				int thisRow = rounded;
				float yPos = -200; // default position for first row to be seen
				yPos += (200 * (thisRow-1));
				if (hole > 10) {
					yPos -= 100;
				}

			//	Vector2 pos = new Vector2 (scrollRect.content.GetChild (0).transform.localPosition.x, yPos);



			//	scrollRect.content.GetChild (0).transform.localPosition = pos;
			}
		} else if (hole != 1 && !PlayerPrefs.HasKey("UnlockedLevel"+hole)) {
			isLocked_ = true;
		} else {
			isLocked_ = false;

		}

		if (hole > 36) {
			if (!PlayerPrefs.HasKey ("extralevels")) {
				padlock.SetActive (true);
				isLocked_ = true;
			} else {
				padlock.SetActive (false);
			}
		}

		isLocked = isLocked_;


		//Texture2D tex = Resources.Load<Texture2D> ("LevelPreviews/" + hole.ToString ()) as Texture2D;
		//GetComponent<RawImage> ().texture = tex;

		Sprite spr = Resources.Load<Sprite> ("LevelPreviews/" + hole.ToString ()) as Sprite;
		thumbnail.sprite = spr;


			/*

		string filePath = "";

//		#if UNITY_EDITOR
		filePath = Application.dataPath + "/Assets/Resources/LevelPreviews/" + hole.ToString () + ".png";
//		#endif
//		#if UNITY_ANDROID
//		filePath = Application.dataPath + "/Assets/Resources/Level Previews/" + hole.ToString () + ".png";
//		#endif
		if (System.IO.File.Exists (filePath)) {
			var bytes = System.IO.File.ReadAllBytes (filePath);
			var tex = new Texture2D (1, 1);
			tex.LoadImage (bytes);
			GetComponent<RawImage> ().texture = tex;
		} else {
			Debug.LogError ("Unable to find image for hole " + hole.ToString ());
		}
*/

		/*
		try {
			GetComponent<Image>().material.mainTexture = Resources.Load("Level Previews/" + hole.ToString() + ".png") as Texture2D;
		} catch {
			Debug.LogError("Unable to find image for hole " + hole.ToString());
		}
		*/
	}
	
	// Update is called once per frame
	void Update () {

		if (a < 1) {
			a += Time.deltaTime/2;

//			Color c1 = buttonImg.color;
			Color c2 = title.color;
			Color c3 = score.color;
			//c1.a = 
		    c2.a = c3.a = a;



			//buttonImg.color = c1;
			title.color = c2;
			score.color = c3;
		}

		if (padlock.activeSelf) {
			if (PlayerPrefs.GetInt ("extralevels") > 0) {
				padlock.SetActive (false);
			}
			
		}

		if (thisHoleSelected) {
			ExpandBorder ();
		}
		
	}
		
	float multiple = 1.6f;
	float borderExpandSpeed = 0.2f;
	Vector3 scale = Vector3.one;
	bool borderDir = false;
	void ExpandBorder() {
		if (!borderDir) {
			if (multiple < 1.65f) {
				multiple += Time.deltaTime * borderExpandSpeed;
				scale = Vector3.one * multiple;
			} else {
				borderDir = true;
			}
		} else {
			if (multiple > 1.6f) {
				multiple -= Time.deltaTime * borderExpandSpeed;
				scale = Vector3.one * multiple;
			} else {
				borderDir = false;
			}
		}

		border.transform.localScale = scale;
	}



	public void LoadLevel() {
		Application.LoadLevel (hole);
	}
}
