﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeUsedParUp : MonoBehaviour {

	public bool destroyParent;

	Color c;
	Color c2;
	float a;

	Image i;
	Text t;

	float scale=1;
	bool swap=false;

	float stillTimer = 0.4f;

	// Use this for initialization
	void Start () {

		c = Color.white;
		a = 1;

		i = GetComponent<Image> ();
		t = transform.GetChild (0).GetComponent<Text> ();
		c2 = t.color;
	}
	
	// Update is called once per frame
	void Update () {

		if (!swap) {
			if (scale < 1.2f) {
				scale += Time.deltaTime * 2;
				transform.localScale = Vector2.one * scale;
			} else {
				swap = true;
			}
			if (a > 0) {
				a -= Time.deltaTime;
				c.a = c2.a = a;
				i.color = c;
				t.color = c2;
			} else {
				Destroy (this.gameObject);
			}
		} else {

			if (stillTimer > 0) {
				stillTimer -= Time.deltaTime;
			}
			else if (scale > 0.7f) {
				scale -= Time.deltaTime/2;
				transform.localScale = Vector2.one * scale;

				if (a > 0) {
					a -= Time.deltaTime;
					c.a = c2.a = a;
					i.color = c;
					t.color = c2;
				} else {
					if (destroyParent) {
						Destroy (transform.parent.gameObject);
					} else {
						Destroy (this.gameObject);
					}
				}
			}
		}


	}
}
