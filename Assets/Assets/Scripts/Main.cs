﻿using UnityEngine;
using System.Collections;

public static class Main {

	public static int parsPerAd=10;
	public static int erasersPerAd = 2;
	public static int skipHolesPerAd = 1;


	// Purchasing Costs
	public static string par10Cost;
	public static string par25Cost;
	public static string par60Cost;
	public static string par150Cost;
	public static string eraser5Cost;
	public static string eraser10Cost;
	public static string eraser15Cost;
	public static string skipHole2Cost;
	public static string skipHole5Cost;
	public static string skipHole7Cost;
	public static string extraLevelsCost;
	public static string removeAdsCost;


	public static int numCourses = 2;
	public static bool gameControllerInit = false;
	public static AdController theAdController;
	public static bool hasPurchaser=false;

	public static int selectedLevel;

	public static bool musicPlayerExists = false;

	public static string courseString {
		get {
			return System.Text.Encoding.UTF8.GetString(thisMap);
		}
	}
	public static int courseNum {
		get {
			return thisMap[0]-65;
		}
	}
	static byte[] thisMap;

	public static void StartMap() {
		if (thisMap == null) {
			thisMap = System.Text.Encoding.UTF8.GetBytes ("A");

			if (PlayerPrefs.GetInt ("extralevels") > 0) {
				numCourses++;
			}
		}
	}

	public static void PlusMap() {
		thisMap[0]++;
	}
}
