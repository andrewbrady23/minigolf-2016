﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

#if UNITY_ANDROID || UNITY_IOS
using UnityEngine.Purchasing;

public class StoreScript : MonoBehaviour {

	public IAppPurchaser iap;

	string parUsage =			"Reduce your standing par in any course! \n > A last moment failsafe";
	string eraserUsage =		"todo (eraser)";
	string skipUsage =			"todo (skip level)";
	string unlockLevelsUsage = 	"todo (unlock levels)";
	string removeAdsUsage = 	"todo (remove ads)";

	public Text whatThisDoes;
	public Text displayAmount;
	public Text displayPrice;

	string selected;

	private int numberToBuy_;
	int numberToBuy {
		get {
			return numberToBuy_;
		}
		set {
			numberToBuy_ = value;
			string price = "";
			switch (selected) {
			case "par":
				if (value == 10) {
					price = Main.par10Cost;
				} else if (value == 25) {
					price = Main.par25Cost;
				} else if (value == 60) {
					price = Main.par60Cost;
				} else if (value == 150) {
					price = Main.par150Cost;
				}
				break;

			case "eraser":
				if (value == 5) {
					price = Main.eraser5Cost;
				} else if (value == 10) {
					price = Main.eraser10Cost;
				} else if (value == 15) {
					price = Main.eraser15Cost;
				}
				break;

			case "skip":
				if (value == 2) {
					price = Main.skipHole2Cost;
				} else if (value == 5) {
					price = Main.skipHole5Cost;
				} else if (value == 7) {
					price = Main.skipHole7Cost;
				}
				break;

			case "unlocklevels":
				price = Main.extraLevelsCost;
				break;

			case "removeads":
				price = Main.removeAdsCost;
				break;

			}
			SetAmountAndPrice (selected, value, price);
		}
	}

	public void ResetInfo() {
		whatThisDoes.text = "";
		displayAmount.text = "";
		displayPrice.text = "";
	}

	public void SelectPar(int amount) {
		whatThisDoes.text = parUsage;
		selected = "par";
		numberToBuy = amount;
	}

	public void SelectEraser(int amount) {
		whatThisDoes.text = eraserUsage;
		selected = "eraser";
		numberToBuy = amount;
	}

	public void SelectSkip(int amount) {
		whatThisDoes.text = skipUsage;
		selected = "skip";
		numberToBuy = amount;
	}

	public void SelectUnlockLevels() {
		whatThisDoes.text = unlockLevelsUsage;
		selected = "unlocklevels";
		numberToBuy = 0;
	}

	public void SelectRemoveAds() {
		whatThisDoes.text = removeAdsUsage;
		selected = "removeads";
		numberToBuy = 0;
	}

	public void BuySelected() {
		switch (selected) {

		case "par":
			iap.BuyPar (numberToBuy_);
			break;
		case "eraser":
			iap.BuyEraser (numberToBuy_);
			break;
		case "skip":
			iap.BuyHoleSkips (numberToBuy_);
			break;
		case "unlocklevels":
			iap.BuyLevels ();
			break;
		case "removeads":
			iap.BuyRemoveAds ();
			break;

		}
	}

	// Use this for initialization
	void Start () {
		FindPrices ();
	}

	void SetAmountAndPrice(string type, int amount, string price) {
		switch (type) {
		case "par":
			whatThisDoes.text = parUsage;
			displayAmount.text = "Amount : " + amount.ToString ();
			displayPrice.text = "Price : " + price;
			break;
		case "eraser":
			whatThisDoes.text = eraserUsage;
			displayAmount.text = "Amount : " + amount.ToString ();
			displayPrice.text = "Price : " + price;
			break;
		case "skip":
			whatThisDoes.text = eraserUsage;
			displayAmount.text = "Amount : " + amount.ToString ();
			displayPrice.text = "Price : " + price;
			break;
		case "unlocklevels":
			whatThisDoes.text = unlockLevelsUsage;
			displayAmount.text = "Amount : " + amount.ToString ();
			displayPrice.text = "Price : " + price;
			break;
		case "removeads":
			whatThisDoes.text = removeAdsUsage;
			displayAmount.text = "Amount : " + amount.ToString ();
			displayPrice.text = "Price : " + price;
			break;
		}
	}

	public Text[] listedPrices;
	void FindPrices() {
		
		listedPrices [0].text = Main.par10Cost;
		listedPrices [1].text = Main.par25Cost;
		listedPrices [2].text = Main.par60Cost;
		listedPrices [3].text = Main.par150Cost;
		listedPrices [4].text = Main.eraser5Cost;
		listedPrices [5].text = Main.eraser10Cost;
		listedPrices [6].text = Main.eraser15Cost;
		listedPrices [7].text = Main.skipHole2Cost;
		listedPrices [8].text = Main.skipHole5Cost;
		listedPrices [9].text = Main.skipHole7Cost;
		//listedPrices [10].text += Main.removeAdsCost;
		//listedPrices [11].text += Main.extraLevelsCost;
	}

}
#endif
