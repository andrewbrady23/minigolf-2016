﻿using UnityEngine;
using System.Collections;

public class SpinWheelPoint : MonoBehaviour {

	public RewardType type;
	public enum RewardType {
		Par1,
		Par2,
		Eraser1,
		Eraser2,
		SkipHole1,
		UnlockLevel
	}
}
