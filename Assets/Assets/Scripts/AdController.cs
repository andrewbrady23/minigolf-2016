﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Heyzap; 
//using AudienceNetwork;
//using GoogleMobileAds.Api;
using System;
//using Amazon.MobileAnalytics.MobileAnalyticsManager;
//using Amazon.CognitoIdentity;

public class AdController : MonoBehaviour {

	private bool isLoaded;
	//BannerView googleBannerView;
//	public MobileAnalyticsManager analyticsManager = null;
//	private CognitoAWSCredentials cognitoProvider = null;

	public Text inMenuAgeText;


	void Start () {
		if (Main.gameControllerInit == true) {
			Destroy (gameObject);
			return;
		} 
		DontDestroyOnLoad (this);
		/*analyticsManager = MobileAnalyticsManager.GetOrCreateInstance(
			new CognitoAWSCredentials("us-east-1:9097c381-21db-4e5b-ba45-44f5fd2cbe7d", Amazon.RegionEndpoint.USEast1),
			Amazon.RegionEndpoint.USEast1,
			"470d45e3e7bb4a429cab03a33d45e1c4");*/

		//cognitoProvider = new CognitoAWSCredentials("us-east-1:9097c381-21db-4e5b-ba45-44f5fd2cbe7d", 	Amazon.RegionEndpoint.USEast1);
		//analyticsManager = MobileAnalyticsManager.GetOrCreateInstance(cognitoProvider,
		//	Amazon.RegionEndpoint.USEast1,
		//	"470d45e3e7bb4a429cab03a33d45e1c4"); //Amazon Mobile Analytics App ID
		
		lastDate = System.DateTime.Now;
		Main.theAdController = this;
		Main.gameControllerInit = true;

	

	}

	//You want this session management code only in one game object
	//that persists through the game life cycles using “DontDestroyOnLoad (transform.gameObject);”
	void OnApplicationFocus(bool focus) {
/*		if (analyticsManager != null) {
			if (focus) {
				analyticsManager.ResumeSession();
			} else {
				analyticsManager.PauseSession();
			}
		}*/
	}

	public void Init() {

		#if UNITY_ANDROID
		if (PlayerPrefs.GetInt ("isAdult") == 0) {
			HeyzapAds.Start ("4daa851b166abdf8bd07826c0247d769", HeyzapAds.FLAG_CHILD_DIRECTED_ADS);
		} else {
			// for now stay with this
		HeyzapAds.Start ("4daa851b166abdf8bd07826c0247d769", HeyzapAds.FLAG_NO_OPTIONS);
		}
		#else 
		HeyzapAds.Start ("4daa851b166abdf8bd07826c0247d769", HeyzapAds.FLAG_NO_OPTIONS);
		#endif

	//	HeyzapAds.ShowMediationTestSuite ();
	//	HZIncentivizedAd.Fetch();
		if (PlayerPrefs.GetInt ("removeads") == 1) {
			return;
		}
	
		HZInterstitialAd.Fetch ();

		ShowBanner ();
	}

	public void ShowBanner () {

		if (PlayerPrefs.GetInt ("removeads") == 1) {
			return;
		}
		#if UNITY_ANDROID
		if  (PlayerPrefs.GetInt ("isAdult") > 0) {
		#endif
			HZBannerShowOptions showOptions = new HZBannerShowOptions ();
			showOptions.Position = HZBannerShowOptions.POSITION_TOP;
			HZBannerAd.ShowWithOptions (showOptions);
		#if UNITY_ANDROID
		} 
		#endif
	//	else {
	//		ShowXPromoBanner ();
	//	}
	}

	public void MoreApps() {
	//	Chartboost.showMoreApps (CBLocation.Default);
	}


	public void ShowXPromoBanner(){
		if (PlayerPrefs.GetInt ("removeads") != 1) {

	/*		AdRequest request = new AdRequest.Builder()
				.AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
				.AddTestDevice("E922C6DCF2B46FE1389F5CCCAB634717")  // Test Device 1.
				.AddExtra("is_designed_for_families","true")
				.TagForChildDirectedTreatment(true)
				.Build();

			googleBannerView = new BannerView(
				"ca-app-pub-8749694805225439/7463443941", GoogleMobileAds.Api.AdSize.SmartBanner, AdPosition.Top);
			// Create an empty ad request.

			// Called when an ad request has successfully loaded.
			googleBannerView.AdLoaded += HandleAdLoaded;
			// Called when an ad request failed to load.
			googleBannerView.AdFailedToLoad += HandleAdFailedToLoad;
			// Load the banner with the request.
			googleBannerView.LoadAd(request);*/

		}
	}

	public void HideBanner() {

		HZBannerAd.Hide ();

//		if (this.googleBannerView != null) {
//			this.googleBannerView.Hide ();
//		}
	}

	public void HandleAdLoaded(object sender, EventArgs args)
	{
		print("HandleAdLoaded event received.");
		// Handle the ad loaded event.
	}

/*	public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("Banner Failed to load: " + args.Message);
		// Handle the ad failed to load event.
	}

	public void InterLoaded(object sender, EventArgs args)
	{
		print("InterLoaded event received.");
		// Handle the ad loaded event.
	}

	public void HandleInterFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("Inter Failed to load: " + args.Message);
		// Handle the ad failed to load event.


	}

	GoogleMobileAds.Api.InterstitialAd interstitial;
	// Use this for initialization
*/
	//void LoadInterstitial ()
	//{
	//	HZVideoAd.Fetch ();

	//	if (PlayerPrefs.GetInt ("isAdult") > 0) {
	//		HZVideoAd.Fetch ();
	//	} else {
	/*		AdRequest request = new AdRequest.Builder ()
			.AddTestDevice (AdRequest.TestDeviceSimulator)       // Simulator.
			.AddTestDevice ("E922C6DCF2B46FE1389F5CCCAB634717")  // Test Device 1.
			.Build ();

	
			interstitial = new GoogleMobileAds.Api.InterstitialAd ("ca-app-pub-8749694805225439/7463443941");
			// Called when an ad request has successfully loaded.
			interstitial.AdLoaded += InterLoaded;
			// Called when an ad request failed to load.
			interstitial.AdFailedToLoad += HandleInterFailedToLoad;
			// Load the interstitial with the request.
			interstitial.LoadAd (request);*/
	//	}

	//}


	// Update is called once per frame
	void Update () {

	}
	static int adViews = 0;

	// Show button

	//Store the current time when it starts
	DateTime currentDate;
	DateTime lastDate;

	public void ShowInterstitial ()
	{
	//	PlayerPrefs.DeleteAll ();
		if (PlayerPrefs.GetInt ("removeads") == 1) {
			return;
		}

		// only show add every x secs.
		currentDate = System.DateTime.Now;
		TimeSpan difference = currentDate.Subtract(lastDate);
		TimeSpan gap = new TimeSpan (0, 2, 45);

		if (difference < gap) {
			return;
		}
		lastDate = System.DateTime.Now;	
		adViews++;


		HZInterstitialAd.Show();

	//	if ((interstitial != null) && interstitial.IsLoaded ()) {
	//		interstitial.Show ();
	// 	}

	}


	void OnDestroy ()
	{
		Debug.Log ("Ad Controller was destroyed!");
	}

}
