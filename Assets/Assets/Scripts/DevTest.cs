﻿using UnityEngine;
using System.Collections;

public class DevTest : MonoBehaviour {

	public void AddLevel() {
		string unlocked = "";
		if (PlayerPrefs.HasKey ("UnlockedLevels")) {
			unlocked = PlayerPrefs.GetString ("UnlockedLevels");
		}
		unlocked += "0";
	}

	public void DeleteExtraSpins() {
		PlayerPrefs.DeleteKey ("ExtraSpins");
	}

	public void DeleteSpinInfo() {
		PlayerPrefs.DeleteKey ("LastTimeSpunWheel");
		PlayerPrefs.DeleteKey ("NumSpins");
	}

	public void DeleteAllPlayerPrefs() {
		PlayerPrefs.DeleteAll ();
	}
}
