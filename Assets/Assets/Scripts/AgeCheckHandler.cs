﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AgeCheckHandler : MonoBehaviour {

	public GameObject ageForm;
	public GameObject gameMenu;
	public GameObject ageButton;

//	public Text inMenuAgeText;
	public GameObject ageTick;

	public GameObject incentivisedAdsButton;

	// Use this for initialization
	void Start () {


		#if UNITY_ANDROID
		if (PlayerPrefs.HasKey ("isAdult")) {
			gameMenu.SetActive (true);
			switch (PlayerPrefs.GetInt ("isAdult")) {
			case 0:
				//	inMenuAgeText.text = "Age: <13";
				incentivisedAdsButton.SetActive(false);
				ageTick.SetActive(true);
				break;
			case 1:
				//	inMenuAgeText.text = "Age: >13";
				incentivisedAdsButton.SetActive(true);
				ageTick.SetActive(false);
				break;
			}
			Main.theAdController.Init ();
			gameMenu.SetActive (true);

		} else {
			ageForm.SetActive (true);
		}
		#else 
		gameMenu.SetActive (true);
		ageForm.SetActive (false);
		ageButton.SetActive (false);
		Main.theAdController.Init ();
		#endif
	}

	public void SwapIsAdult() {
		switch (PlayerPrefs.GetInt ("isAdult")) {
		case 0:
			PlayerPrefs.SetInt ("isAdult", 1);
		//	inMenuAgeText.text = "Age: >13";
			incentivisedAdsButton.SetActive(true);
			ageTick.SetActive(false);
			break;
		case 1:
			PlayerPrefs.SetInt ("isAdult", 0);
			//	inMenuAgeText.text = "Age: <13";
			incentivisedAdsButton.SetActive(false);
			ageTick.SetActive(true);
			break;
		}
	}

	public void SetIsAdult(bool isAdult) {

		incentivisedAdsButton.SetActive(isAdult);

		if (isAdult) {
			PlayerPrefs.SetInt ("isAdult", 1);

		} else {
			PlayerPrefs.SetInt ("isAdult", 0);

		}
		Main.theAdController.Init ();
	}

}
