﻿using UnityEngine;
using System.Collections;

public class TakeScreenshot : MonoBehaviour {

	public int courseNum;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
		{
			//string fn = Application.dataPath;
			//fn = fn.Substring(0,fn.Length-6);
			//int index = screenNom;

			//fn += Application.loadedLevelName + index.ToString() + ".png";
			string fn = Application.dataPath + "/Assets/Resources/Level Previews/" + (Application.loadedLevel+(18*(courseNum-1))).ToString() + ".png";
			Debug.Log ("captureScreenshot:" + fn);
#if UNITY_5_3_OR_NEWER
            ScreenCapture.CaptureScreenshot(fn);
#else
            Application.CaptureScreenshot(fn);
#endif
			//screenNom++;
		}
	}
}
