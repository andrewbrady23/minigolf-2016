﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CourseOpening : MonoBehaviour {

	public GameObject[] levelObjectsToLoad;
	public Transform cam;
	float camRotY;
	GameObject parent;

	Text toFade;

	Color colour;
	float alpha=1;

	// Use this for initialization
	void Start () {
		toFade = GetComponent<Text> ();
		colour = toFade.color;

		parent = transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		alpha -= Time.deltaTime/3;
		colour.a = alpha;
		toFade.color = colour;

		camRotY += Time.deltaTime*20;
		cam.rotation = Quaternion.Euler (new Vector3(0,camRotY,0));

		if (alpha <= 0) {
			foreach (GameObject g in levelObjectsToLoad) {
				g.SetActive (true);
				parent.gameObject.SetActive (false);
			}
		}
	}
}
