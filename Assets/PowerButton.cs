﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PowerButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {


	BallScript ballScript;

	public Slider powerSlider;

	void OnEnable() {
		ballScript = GameObject.Find("Ball").GetComponent<BallScript>();
	}

	void OnDisable() {
		ballScript = null;
	}

	public void SetupSlider() {
		//Debug.Log("bleh");
		ballScript.GrabSlider (this);
	}

	public void UpdatePowerSlider(float val) {
		powerSlider.value = val;
	}

	public void OnPointerDown(PointerEventData evd) {
		//Debug.Log ("Power Down");
		ballScript.StartPower();
	}

	public void OnPointerUp(PointerEventData evd) {
		//Debug.Log ("Power Up");
		ballScript.EndPower();
	}
}
