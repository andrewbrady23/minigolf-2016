//#define PHOTON_MULTIPLAYER

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using FMG;

/// <summary>
/// Mini Golf Play state.
/// </summary>
public class PlayState : MonoBehaviour {

	#region variables
	//a ref of the gamescript
	private GameScript m_gameScript;
	
	public Text holeGT;
	public Text scoreGT;
	
	private bool m_gameStart = false;
	public GameObject pauseState;
	public GameObject playState;
	public GameObject controlsPanel;
	private bool m_gamestart=false;
	private bool m_gameover=false;

	public GameObject skipHoleButton;
	public GameObject eraserButton;

	#endregion
	void Start()
	{
		
		m_gameScript = (GameScript)GameObject.FindObjectOfType(typeof(GameScript));		
		#if PHOTON_MULTIPLAYER
		
		if(PhotonNetwork.offlineMode)
		{
			Destroy(GameObject.Find("CLOCK"));
		}
		#endif
	}
	

	public void onPause ()
	{
		GameManager.enterState(GameScript.State.PAUSE.ToString());
		GameConfig.setPaused(true);
		FMG.Constants.fadeInFadeOut(pauseState,playState);
		controlsPanel.SetActive (false);

		if (PlayerPrefs.HasKey ("HoleSkip") && PlayerPrefs.GetInt ("HoleSkip") > 0) {
			if (PlayerPrefs.GetString ("UnlockedLevels").Length < Application.loadedLevel) {
				skipHoleButton.transform.GetChild (0).GetComponent<Text> ().text = ("Skip (" + PlayerPrefs.GetInt ("HoleSkip") + ")");
				skipHoleButton.GetComponent<Image> ().color = Color.white;
				//skipHoleButton.SetActive (true);
			}
		}

		if (PlayerPrefs.HasKey ("Erasers") && PlayerPrefs.GetInt ("Erasers") > 0) {
			eraserButton.transform.GetChild (0).GetComponent<Text> ().text = ("Erase (" + PlayerPrefs.GetInt ("Erasers") + ")");
			eraserButton.GetComponent<Image> ().color = Color.white;
			//eraserButton.SetActive (true);
		}
		
	}

	public  void OnEnable()
	{
		BaseGameManager.onGameOver += onGameOver;
		BaseGameManager.onGameStart += onGameStart;
	}
	public  void OnDisable()
	{
		BaseGameManager.onGameOver -= onGameOver;
		BaseGameManager.onGameStart -= onGameStart;
	}
	public void onGameStart()
	{
		m_gamestart=true;
	}
	public void onGameOver(bool vic)
	{
		m_gameover=true;
	}
	public 	 void Update()
	{
		int holeIndex = m_gameScript.getHoleNomUsingCourse();
		if(holeGT)
		{
			holeGT.text = holeIndex.ToString();
		}
		
		
		
		//display the number of strokes.
		int nomStrokes = m_gameScript.getNomStrokes();		
		if(scoreGT)
		{
			scoreGT.text = nomStrokes.ToString();
		}

		if(Input.GetKeyDown(KeyCode.Escape) && m_gameover==false && m_gamestart)
		{
			onPause();
		}
	}
}
