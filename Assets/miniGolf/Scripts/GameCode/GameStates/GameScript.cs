//#define PHOTON_MULTIPLAYER
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using Amazon.MobileAnalytics.MobileAnalyticsManager;
//using Amazon.CognitoIdentity;
//using Heyzap; 
/// <summary>
/// The mini golf gameScript.
/// </summary>
public class GameScript : MonoBehaviour {
	#region variables
	///the currentNumber of shots 
	public int currentNumberOfStrokes = 0;
	
	///the maximum number of strokes
	public int maxNomStrokes = 8;
	/// <summary>
	/// The number of holes per course
	/// </summary>
	public static int HOLES_PER_COURSE = 18 ;
	static int originalHoles = HOLES_PER_COURSE;

	/// the audio clip to play when the player has not gotten it in the hole but taken the maxmium number of strokes!	
	public AudioClip big10AC;
	
	///the audio clip to play when the ball has fallen in the water
	public AudioClip falloutAC;
	
	///the sound to play when the ball has gotten it in the hole
	public AudioClip winAC;

	
	///did we get in the hole before we ran out of strokes.
	private bool m_failedToGetInHole = false;
	
	///a ref to the ballscript
	private BallScript m_ballScript;

	public Text parText;
	public Text scoreText;
	public Text endOfLevelScore;
	Color scoreTextColour;

	//public GameObject failWindow;
	public Text parInfo;

	public GameObject continueButton;
	public GameObject replayButton;

	public GameObject buyParUp;

	public GameObject usedParUp;
	public Transform canvas;

	public GameObject eraserButton;
	public Text eraserCount;

	public GameObject skipHoleButton;
	public Text skipHoleCount;

	public GameObject controlsPanel;
	
	public enum State
	{
		INIT=0,
		PLAY=1,
		PAUSE=2,
		SHOWSCORE=3,
		SUBMTSCORE=4,
		WATER_HAZARD=8,
		ROLL=16
	};
	
	///our current state
	public State m_state;
	
	///the gui skin to use
	public GUISkin guiSkin0;
	
	///the list of states -- should correspond with the gamescript states.
	public GameObject[] states;
	
	private int m_par;
	
	///the default ambient color.
	static float color = 51f / 255f;
	public Color ambientColor =  new Color(color,color,color,1f);
	
	/// <summary>
	/// The course names.
	/// </summary>
	public string[] courseNames = {"Course A", "Course B", "Course C", "Course D"};
	private static int m_totalPar = 0;
//	private int m_totalScore;
	#endregion


	public GameObject[] parBitsToDisableOnReplay;
	public Text youAreReplayingWarning;
	public Text parInfoIfWon;
	bool showReplayWarning;
	bool replayFade=false;
	Color replayColour;
	float replayA;
	

	void OnEnable()	
	{
		GameManager.onEnterState += onEnterState;
		GolfManager.onTimesUp += onTimesUp;

		BaseGameManager.onGameStart += startGame;
	}
	void OnDisable()
	{
		GameManager.onEnterState -= onEnterState;
		GolfManager.onTimesUp -= onTimesUp;
		BaseGameManager.onGameStart -= startGame;
	}
	
	void onEnterState(string state)
	{
		if(state.Equals(State.PLAY.ToString() ))
		{
			m_ballScript.setMode( BallScript.BallMode.AIM);
			m_state = GameScript.State.PLAY;
		}
	}
	
	public int getParAtHole(int par)
	{
		return m_par;
	}

	private GameObject m_scoreStateGO;
	public void Awake()
	{
		transform.position = Vector3.zero;
		Main.StartMap();
		RenderSettings.ambientLight = ambientColor;

		bool hasPlayedBefore = (PlayerPrefs.GetString ("UnlockedLevels").ToCharArray ().Length >= Application.loadedLevel);

		//unpause the game
		GameConfig.setPaused(false);
		ScoreState ss = (ScoreState)GameObject.FindObjectOfType(typeof(ScoreState));
		if(ss)
		{
			m_scoreStateGO = ss.gameObject;
		}
		//if its levle 1 reset the scores.
		int holeIndex = getHoleNomUsingCourse();
//		Debug.Log("holeIndex"+holeIndex);
		if(holeIndex==1)
		{
			setScoreForHole(getHoleNomUsingCourse(),currentNumberOfStrokes);
			setTotalScore(0);
//			m_totalScore=0;
			m_totalPar = 0;
		}else{

//			m_totalScore = getTotalScore();
		}
		ParScript ps = (ParScript)GameObject.FindObjectOfType(typeof(ParScript));
		if(ps)
		{
			m_par = ps.par;
			setParForHole(getHoleNomUsingCourse(),m_par);
			m_totalPar+=m_par;

		}else{
			setParForHole(getHoleNomUsingCourse(),m_par);
		}

		if (!hasPlayedBefore) {
			string suffix = (PlayerPrefs.HasKey ("sparepar") && PlayerPrefs.GetInt ("sparepar") > 0) ? " (+" + PlayerPrefs.GetInt ("sparepar") + ")" : "";
			parText.text = m_par.ToString () + suffix;
		}

		//get a ref to the gamescript!
		GameObject go = GameObject.FindWithTag("Player");
		if(go)
		{
			m_ballScript = go.GetComponent<BallScript>();
		}	
		
		GameManager.enterState( GameScript.State.INIT.ToString() );



		if (hasPlayedBefore) {
			showReplayWarning = true;
			replayColour = youAreReplayingWarning.color;
		}

		scoreTextColour = scoreText.color;



	}


	public void Update()
	{
		bool paused = GameConfig.getPaused();

		if(m_state==State.PLAY || m_state==State.PAUSE)
		{
			if(paused==true)
			{
				
				m_state = State.PAUSE; 
				Screen.lockCursor =false;
			}else{
				m_state = State.PLAY; 
			}
		}

		if (showReplayWarning) {
			if (!replayFade) {
				if (replayA < 1) {
					replayA += Time.deltaTime;
					replayColour.a = replayA;
					youAreReplayingWarning.color = replayColour;
				} else {
					StartCoroutine ("SetReplayToFadeOut");
				}
			} else {
				if (replayA > 0) {
					replayA -= Time.deltaTime;
					replayColour.a = replayA;
					youAreReplayingWarning.color = replayColour;
				} else {
					showReplayWarning = false;
				}
			}
		}
	}

	public void WatchAdForPar() {
#if UNITY_ANDROID || UNITY_IOS
        GameObject.FindObjectOfType<IAppPurchaser> ().WatchAdForPar ();
#endif
	}

	public Text[] listedPrices;
	public void SetPrices() {
		listedPrices [0].text += Main.par10Cost;
		listedPrices [1].text += Main.par25Cost;
		listedPrices [2].text += Main.par60Cost;
		listedPrices [3].text += Main.par150Cost;
	}

	IEnumerator SetReplayToFadeOut() {
		yield return new WaitForSeconds (7);
		replayFade = true;
	}

	public void BuyPar(int amount) {
#if UNITY_ANDROID || UNITY_IOS
        GameObject.Find ("InAppPurchasingController").GetComponent<IAppPurchaser> ().BuyPar ( amount);
#endif
	}
	public void BuyEraser(int amount) {
#if UNITY_ANDROID || UNITY_IOS
        GameObject.Find ("InAppPurchasingController").GetComponent<IAppPurchaser> ().BuyEraser ( amount);
#endif
	}
	public void BuyHoleSkips(int amount) {
#if UNITY_ANDROID || UNITY_IOS
        GameObject.Find ("InAppPurchasingController").GetComponent<IAppPurchaser> ().BuyHoleSkips (amount);
#endif
	}
	public void ParUp(int amount) {
//		Debug.Log ("purchased +par");

		int pars = 0;
		if (PlayerPrefs.HasKey ("sparepar")) {
			pars += PlayerPrefs.GetInt ("sparepar");
		}

		//pars += Main.parsPerPurchase-1;
		pars += amount;
		m_par ++;

		PlayerPrefs.SetInt ("sparepar", pars);
		string suffix = (PlayerPrefs.HasKey("sparepar") && PlayerPrefs.GetInt("sparepar") > 0) ? " (+" + PlayerPrefs.GetInt("sparepar") + ")" : "";
		parText.text = m_par.ToString() + suffix;


//		Debug.Log (PlayerPrefs.GetInt("sparepar"));
	}

	public void ContinueOverPar() {
		buyParUp.SetActive (false);
		Time.timeScale = 1;
	}

	public void ExitFromPar() {

		Main.theAdController.ShowBanner ();

		if (m_ballScript.hasWon() && currentNumberOfStrokes <= m_par) {
			string unlocked = "";
			if (PlayerPrefs.HasKey ("UnlockedLevels")) {
				unlocked = PlayerPrefs.GetString ("UnlockedLevels");
			}
			if (unlocked.Length < Application.loadedLevel) {
				unlocked += "0";
				PlayerPrefs.SetString ("UnlockedLevels", unlocked);
			}
		}

		string thisKey = "Level"+Application.loadedLevel.ToString()+"Score";

		if (PlayerPrefs.HasKey (thisKey)) {
			if (currentNumberOfStrokes < PlayerPrefs.GetInt (thisKey)) {
				PlayerPrefs.SetInt (thisKey, currentNumberOfStrokes);
			}
		} else {
			PlayerPrefs.SetInt (thisKey, currentNumberOfStrokes);
		}


		Application.LoadLevel (0);
		Time.timeScale = 1;
	}

	public void SetScoreAtEnd() {

		//CustomEvent customEvent = new CustomEvent("LevelComplete " + Application.loadedLevel.ToString ());

		// Add attributes
		//customEvent.AddMetric("Strokes",currentNumberOfStrokes);

		int underPar = m_par - currentNumberOfStrokes;
		endOfLevelScore.text = currentNumberOfStrokes.ToString ();
		if (currentNumberOfStrokes <= m_par) {
		//	customEvent.AddAttribute("Successful","True");
			parInfo.text = "";
			endOfLevelScore.color = Color.green;


			if (underPar > 0) {
				int par = PlayerPrefs.GetInt ("sparepar");
				par += underPar;
				PlayerPrefs.SetInt ("sparepar", par);

				string t = endOfLevelScore.text;
				t += (" (+" + underPar + ")");

				endOfLevelScore.text = t;


				if (underPar == 1) {
					parInfo.text = "Scored under par and\ngained a bonus par!";
				} else {
					parInfo.text = "Scored under par and\ngained " + underPar + " bonus par!";
				}

			}



		} else {
			//customEvent.AddAttribute("Successful","False");
			Vector3 pos = replayButton.transform.position;
			pos.x = Screen.width/2;

			replayButton.transform.position = pos;
			continueButton.SetActive (false);

			endOfLevelScore.color = Color.red;
		}

		// Record the event
		//if (Main.theAdController.analyticsManager != null) {
		//	Main.theAdController.analyticsManager.RecordEvent (customEvent);
		//}
	}

	public void goBackFromFail() {
		string thisKey = "Level"+Application.loadedLevel.ToString()+"Score";

		if (PlayerPrefs.HasKey (thisKey)) {
			if (currentNumberOfStrokes < PlayerPrefs.GetInt (thisKey)) {
				PlayerPrefs.SetInt (thisKey, currentNumberOfStrokes);
			}
		} else {
			PlayerPrefs.SetInt (thisKey, currentNumberOfStrokes);
		}

		Application.LoadLevel (0);
	}

	public void Erase() {
		int erasers = PlayerPrefs.GetInt ("Erasers");
	

		if (erasers > 0) {
			//CustomEvent customEvent = new CustomEvent("Eraser" );

			// Add attributes
			//customEvent.AddMetric("Level", Application.loadedLevel);
			//customEvent.AddMetric("Eraser Count",erasers);

			erasers--;
			PlayerPrefs.SetInt ("Erasers", erasers);
			scoreText.color = scoreTextColour;
			currentNumberOfStrokes--;
			eraserButton.SetActive (false);
			buyParUp.SetActive (false);
			Time.timeScale = 1;

			// Record the event
			//if (Main.theAdController.analyticsManager != null) {
			//	Main.theAdController.analyticsManager.RecordEvent (customEvent);
			//}
		}
	}

	public void SkipScene() {
		int numSkips = PlayerPrefs.GetInt ("HoleSkip");
		if (numSkips > 0) {
			string unlocked = "";
			if (PlayerPrefs.HasKey ("UnlockedLevels")) {
				unlocked = PlayerPrefs.GetString ("UnlockedLevels");
			}
			if (unlocked.Length < Application.loadedLevel) {
				unlocked += "0";
				PlayerPrefs.SetString ("UnlockedLevels", unlocked);
			}

			string thisKey = "Level" + Application.loadedLevel.ToString () + "Score";
			PlayerPrefs.SetInt (thisKey, 1000);

			numSkips--;

			PlayerPrefs.SetInt ("HoleSkip", numSkips);

			Application.LoadLevel (Application.loadedLevel + 1);

		} else {
			Debug.LogError ("No hole skips available! This line shouldn't be reached");
		}
	}
	
	//load the next scene
	public void nextScene(bool replay)
	{
		
		if (Main.theAdController) {
			if (PlayerPrefs.GetInt ("removeads") != 1) {
				Main.theAdController.ShowInterstitial ();
			}


		}
//			HZInterstitialAd.Show ();




		if (!replay) {
			if (currentNumberOfStrokes <= m_par) {
				string unlocked = "";
				if (PlayerPrefs.HasKey ("UnlockedLevels")) {
					unlocked = PlayerPrefs.GetString ("UnlockedLevels");
				}
				if (unlocked.Length < Application.loadedLevel) {
					unlocked += "0";
					PlayerPrefs.SetString ("UnlockedLevels", unlocked);
				}
			}
			string thisKey = "Level" + Application.loadedLevel.ToString () + "Score";

			if (PlayerPrefs.HasKey (thisKey)) {
				if (currentNumberOfStrokes < PlayerPrefs.GetInt (thisKey)) {
					PlayerPrefs.SetInt (thisKey, currentNumberOfStrokes);
				}
			} else {
				PlayerPrefs.SetInt (thisKey, currentNumberOfStrokes);
			}
		}


		Application.LoadLevel (Application.loadedLevel + (replay ? +0 : +1) );



	//	bool nextLevel = goToNextLevel();
	//	if(nextLevel==false)
	//	{
	//		m_state = State.SUBMTSCORE;
	//	}		
	}

	public void victory()
	{
//			Debug.Log ("victory: m_state: "+m_state);
		if(m_state == State.PAUSE || m_state == State.PLAY)
		{			
//			m_masterCamera.setState( MasterCamera.State.END);
			setScoreForHole(getHoleNomUsingCourse(),currentNumberOfStrokes);
			m_state = State.SHOWSCORE;
			
//			Debug.Log ("enterScoreState" + m_scoreStateGO.name);

//			MenuStateManager.enterStateUsingGameObject(m_scoreStateGO);
			finishHole();


//			Debug.Log ("victory"+currentNumberOfStrokes);
			if(m_failedToGetInHole==false)			
			{
				if(winAC)
				{
					GetComponent<AudioSource>().PlayOneShot (winAC);
				}
			}else{
				if(big10AC)
				{
					GetComponent<AudioSource>().PlayOneShot (big10AC);
				}
			}
		}else{
			setScoreForHole(getHoleNomUsingCourse(),currentNumberOfStrokes);
		}
	}
	public void OnGUI()
	{
		GUI.skin = guiSkin0;
		

	}

	//return to the main menu
	public void returnToMain()
	{
#if PHOTON_MULTIPLAYER

			GolfManager.loadLevel(0,PhotonNetwork.player.ID);
#else
			Application.LoadLevel( 0 );
#endif

	}
	
	//give up!
	public void giveUp()
	{
		takeMaximumNumberOfStrokes();
		GameConfig.setPaused(false);
		BaseGameManager.gameover(true);
		//GolfManager.setPlayersScore(PhotonNetwork.player.ID,getTotalScore());
		//GolfManager.finishHole();
	}
	
	//unpause the game
	public void resume()
	{
		GameConfig.setPaused( false );
	}
		
	//start the game.
	public void startGame()
	{
//		m_masterCamera.setState( MasterCamera.State.ON_TEE);

		if (showReplayWarning) {
			replayFade = true;
		}
		m_ballScript.setMode(BallScript.BallMode.AIM);
		m_state = State.PLAY;		
	}	
	
	//the ball has been placed on the tee, now move into aim mode.
	public void placeBall()
	{
//		m_masterCamera.setState( MasterCamera.State.AIM);
		m_ballScript.setMode(BallScript.BallMode.AIM);
		m_state = State.PLAY;	
	}
	
	//the ball has fallen in the water
	public void onFallOutStart()
	{
		//check to ensure that the ball has not already gotten in the hole 
		if(m_ballScript.hasWon()==false)
		{
			//change the master camera to water state
			GameManager.enterState(GameScript.State.WATER_HAZARD.ToString());
			Debug.Log ("EnterWater");
			//if there is a fallout audio clip play it.
			if(GetComponent<AudioSource>() && falloutAC)
			{
				Debug.Log ("EnterWater2");
				GetComponent<AudioSource>().PlayOneShot (falloutAC);
			}
		}
	}
	
	//change to aim mode 
	public void onAimMode()
	{
		m_ballScript.lookAtHole();
		m_ballScript.setMode(BallScript.BallMode.AIM);
		GameManager.enterState(GameScript.State.PLAY.ToString());
	}
	

	//the ball has "stopped" add a stroke
	public void onStrokeEnd()
	{

		//if we have a ball and we have exceeded the maximum number of strokes set it a victory but indicate we didnt finish! 
		/*if(currentNumberOfStrokes>(maxNomStrokes-1))
		{
			m_failedToGetInHole=true;
			GameManager.enterState(GameScript.State.SHOWSCORE.ToString());
		//	failWindow.SetActive (true);

		}else{*/
			GameManager.enterState(GameScript.State.PLAY.ToString());
		//}



		if (currentNumberOfStrokes >= m_par) {
			scoreText.color = Color.red;

			bool hasPlayedBefore = (PlayerPrefs.GetString ("UnlockedLevels").ToCharArray ().Length >= Application.loadedLevel);

			if (!m_ballScript.hasWon ()) {
				if (hasPlayedBefore || !PlayerPrefs.HasKey ("sparepar") || PlayerPrefs.GetInt ("sparepar") == 0) {
					buyParUp.SetActive (true);
					controlsPanel.SetActive (false);

					if (hasPlayedBefore) {
						foreach (GameObject g in parBitsToDisableOnReplay) {
							g.SetActive (false);
						}

						string thisKey = "Level"+Application.loadedLevel.ToString()+"Score";
						int score = PlayerPrefs.GetInt (thisKey);

						if (score != 1000) {
							parInfoIfWon.text = ("Beat Your Best! - " + score + " shot"	+ (score > 1 ?	"s!" : "!"));
						} else {
							parInfoIfWon.text = "You Skipped This Hole Last Time!\nTry to finish under par!";
						}
					}

					int erasers = PlayerPrefs.GetInt ("Erasers");
					if (erasers > 0) {
						eraserCount.text = erasers.ToString ();
						eraserButton.SetActive (true);
					}

					int holeSkips = PlayerPrefs.GetInt ("HoleSkip");
					if (holeSkips > 0) {
						skipHoleCount.text = holeSkips.ToString ();
						skipHoleButton.SetActive (true);
					}

					Time.timeScale = 0;
				} else {
					m_par++;
					PlayerPrefs.SetInt ("sparepar", PlayerPrefs.GetInt ("sparepar") - 1);
					GameObject warning = GameObject.Instantiate (usedParUp, Vector3.zero, Quaternion.identity) as GameObject;
					warning.transform.parent = canvas;
					warning.transform.localPosition = Vector3.zero;
					string suffix = (PlayerPrefs.HasKey ("sparepar") && PlayerPrefs.GetInt ("sparepar") > 0) ? " (+" + PlayerPrefs.GetInt ("sparepar") + ")" : "";
					parText.text = m_par.ToString () + suffix;
				}
			}

		}

	}
		
	
	
	//the ball is rolling!
	public void onRollMode()
	{
//		m_masterCamera.setState( MasterCamera.State.ROLL );
		currentNumberOfStrokes++;
		int courseLevel = getHoleNomUsingCourse();
	
		updateScores( courseLevel );	
	}
	private bool m_finishedHole=false;
	public void onTimesUp()
	{
		if(m_finishedHole==false)
		{
			giveUp();
		}
	}
	public void finishHole()
	{
		if(m_finishedHole==false)
		{
#if PHOTON_MULTIPLAYER
					GolfManager.setPlayersScore(PhotonNetwork.player.ID,getTotalScore());
#else
				GolfManager.setPlayersScore(0,getTotalScore());
#endif
			GolfManager.finishHole();
			m_finishedHole=true;
		}
	}
	//take the maximum number of strokes
	public void takeMaximumNumberOfStrokes()
	{
		currentNumberOfStrokes= maxNomStrokes;
		m_failedToGetInHole=true;
		if(m_ballScript)
			m_ballScript.victory();
		finishHole();

	}


	//returns the current state
	public State getState()
	{
		return m_state;
	}
	
	//returns the current hole
	public int getHole()
	{
		return Application.loadedLevel;
	}
	
	//returns the current number of strokes!
	public int getNomStrokes()
	{
		return currentNumberOfStrokes;
	}
	public int getCourseIndex()
	{
		int course = 18;
		int cousreIndex = 0;
		while(course < Application.loadedLevel)
		{
			course+=18;
			cousreIndex++;
		}
		return cousreIndex;
	}
	public int getHoleNomUsingCourse()
	{
		return Application.loadedLevel;
	}
	public bool goToNextLevel()
	{
		bool rc = true;
		int currentLevel = Application.loadedLevel;
		
		int courseLevel = getHoleNomUsingCourse();
		Debug.Log ("courseLevel" + courseLevel);
		updateScores( courseLevel );
		
		TransitionScript ts = (TransitionScript)GameObject.FindObjectOfType(typeof(TransitionScript));
		Constants.setLastSceneIndex(currentLevel+1);

		if (courseLevel % 18 == 0) {
			Main.PlusMap ();
		}

		if(courseLevel+1 < HOLES_PER_COURSE+1)
		{
			if(ts)
			{
				loadLevel(ts.sceneIndex);
				//Application.LoadLevel(ts.sceneIndex);
			}else{
				loadLevel(currentLevel+1);
				//Application.LoadLevel( currentLevel+1 );
			}
		}else{
			if(Constants.getPracticeMode()==false)
			{
				BaseGameManager.submitSate();
				GameManager.enterState(GameScript.State.SUBMTSCORE.ToString() );
			}else{ // load next course list here



				//if (Main.courseNum < Main.numCourses+1) {

				//	courseLevel = 0;
					//HOLES_PER_COURSE += originalHoles;
				//	loadLevel (currentLevel + 1);
				//} else {
					loadLevel (0);
				//}
			}
			rc = false;
		}
		return rc;
	}
	void loadLevel(int levelToLoad)
	{
#if PHOTON_MULTIPLAYER
			GolfManager.loadLevel(levelToLoad,PhotonNetwork.player.ID);

#else
			Application.LoadLevel(levelToLoad);
#endif
	}

	void updateScores(int curLevel)
	{

		setScoreForHole( curLevel-(18 * Main.courseNum), currentNumberOfStrokes);


		if (currentNumberOfStrokes > m_par) {
			
		}
	}
	
	//sets the hole at the given index
	public void setParForHole(int holeIndex,
											int score)
	{
		string id = "GolfTotalPar" + holeIndex.ToString();
		PlayerPrefs.SetInt( id,score);
	}
	
	//gets teh score at the current hole
	public int getParForHole(int holeIndex)
	{
		string id = "GolfTotalPar" + holeIndex.ToString();
		int rc  = PlayerPrefs.GetInt(id ,0);
		return rc;
	}

	//sets the hole at the given index
	public void setScoreForHole(int holeIndex,
								int score)
	{
		string id = "GolfTotalHole" + holeIndex.ToString();
		PlayerPrefs.SetInt( id,score);
	}
	
	//gets teh score at the current hole
	public int getTotalScoreAtHole(int holeIndex)
	{
		string id = "GolfTotalHole" + holeIndex.ToString();
		int rc  = PlayerPrefs.GetInt(id ,0);
		return rc;
	}
	public int getTotalPar()
	{
		int totalPar = 0;
		int currentHole = getHoleNomUsingCourse();
		for(int i=1; i<19; i++)
		{
			if (i <= currentHole)
			{
				totalPar += getParForHole(i);
			}
		}
		return totalPar;
	}
	public int getTotalScore()
	{
		int totalPar = 0;
		int currentHole = getHoleNomUsingCourse();
		for(int i=1; i<19; i++)
		{
			if (i <= currentHole)
			{
				totalPar += getTotalScoreAtHole(i);
			}
		}
		return totalPar;
	}
	
	//sets the total score
	public void setTotalScore(int score)
	{

		PlayerPrefs.SetInt( "GolfTotalHole",score);
	}
	public int getPar()
	{
		return m_par;
	}
	
}

