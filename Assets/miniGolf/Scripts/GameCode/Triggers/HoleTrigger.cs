﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// When the ball leaves the trigger volume it will reset the ball to it's current hole
/// </summary>
public class HoleTrigger : MonoBehaviour
{
    private GameObject playerObjRef;

    // Start is called before the first frame update
    void Start()
    {
        playerObjRef = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerExit(Collider other)
    {
        //Check if player
        if (other.gameObject == playerObjRef)
        {
            BallScript ballRef = other.gameObject.GetComponent<BallScript>();

            if(ballRef)
            {
                ballRef.fallout();
            }
        }
    }
}
