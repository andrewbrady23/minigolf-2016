﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CGA.Controller;

public class GolfController : MonoBehaviour
{
    public KeyCode AimLeft = KeyCode.LeftArrow;
    public KeyCode AimRight = KeyCode.RightArrow;
    public float RotateSpeed = 40.0f;
    public KeyCode ShootKey = KeyCode.Space;

    [Header("References")]
    public BallScript ballRef;

    private CGA.Controller.ControllerBase ControllerRef;
    

    // Start is called before the first frame update
    void Start()
    {
        if (ballRef == null)
            ballRef = FindObjectOfType<BallScript>();

        ControllerUpdater ControllerInstance = ControllerUpdater.instance;
        if(ControllerInstance == null)
            ControllerInstance = new GameObject("[ControllerUpdater]").AddComponent<ControllerUpdater>();

        ControllerRef = ControllerInstance.GetLastActiveController();

        ControllerInstance.ActiveControllerChanged += OnControllerChanged;
    }

    #region Controller Stuff
    void OnControllerChanged(CGA.Controller.ControllerBase oldController, CGA.Controller.ControllerBase newController)
    {
        ControllerRef = newController;
    }
    #endregion

    // Update is called once per frame
    void Update()
    {
        if(ControllerRef != null)
        {
            float turnValueStick = ControllerRef.GetLeftStick().x;
            float turnValueDpad = (ControllerRef.GetButton(ControllerBase.Buttons.Left) ? -1f : 0f) + (ControllerRef.GetButton(ControllerBase.Buttons.Right) ? 1f : 0f);
            float turnValue = Mathf.Abs(turnValueStick) > Mathf.Abs(turnValueDpad) ? turnValueStick : turnValueDpad;

            if(ballRef)
            {
                ballRef.externalAimAxis = turnValue * Time.smoothDeltaTime * RotateSpeed;

                if(ControllerRef.GetButtonDown(ControllerRef.SelectButton))
                {
                    ballRef.StartPower();
                }
                else if(ControllerRef.GetButtonUp(ControllerRef.SelectButton))
                {
                    ballRef.EndPower();
                }
            }
        }
    }
}
