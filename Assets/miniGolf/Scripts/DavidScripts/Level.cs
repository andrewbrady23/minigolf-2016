﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using CGA.Controller;

namespace CGA.Golf
{
    public class Level : MonoBehaviour
    {
        /// <summary>
        /// The holes for this level
        /// </summary>
        [Tooltip("The Holes for this level")]
        public Hole[] Holes;

        /// <summary>
        /// Reference to the ball in the scene
        /// </summary>
        [Tooltip("Reference to the Ball in the Scene")]
        public BallScript BallRef;

        public UnityEngine.Timeline.TimelineAsset IntroTimeline;

        /// <summary>
        /// Reference to the current hole being played
        /// </summary>
        public Hole CurrentHole { get { return Holes[m_currentHoleIndex]; } }

        /// <summary>
        /// The current index for a hole
        /// </summary>
        private int m_currentHoleIndex = 0;

        /// <summary>
        /// True if a timeline is currently being played
        /// </summary>
        private bool isPlayingTimeline = false;

        /// <summary>
        /// Reference to the current controller. Only valid when Timeline is playing
        /// </summary>
        private ControllerBase currentController;

        /// <summary>
        /// Reference to the Playable director on this object
        /// </summary>
        private PlayableDirector DirectorRef;

        private void Start()
        {
            if (Holes.Length <= 0)
            {
                this.enabled = false;
                Debug.LogError("Can not start game. No Holes assigned in the Hole Manager.", gameObject);
                return;
            }

            //Play Intro Sequence
            if(IntroTimeline != null)
            {
                DirectorRef = GetComponent<PlayableDirector>();

                if (DirectorRef == null)
                    DirectorRef = gameObject.AddComponent<PlayableDirector>();

                DirectorRef.stopped += OnDirectorStopped;
                DirectorRef.Play(IntroTimeline, DirectorWrapMode.None);

                isPlayingTimeline = true;

                //Get Controller References
                ControllerUpdater controllerManager = ControllerUpdater.instance;
                controllerManager.ActiveControllerChanged += OnControllerChanged;
                currentController = controllerManager.GetLastActiveController();
            }
            else
            {
                StartLevel();
            }
        }

        private void StartLevel()
        {
            if (BallRef == null)
            {
                BallRef = FindObjectOfType<BallScript>();
            }

            //Start the first hole and listen for when this hole is finished
            Holes[m_currentHoleIndex].StartHole(BallRef);
            Holes[m_currentHoleIndex].HoleFinished += HoleFinished;
        }

        private void Update()
        {
            if(isPlayingTimeline)
            {
                if(currentController != null && currentController.GetButtonDown(currentController.SelectButton))
                {
                    DirectorRef.Stop();
                }
            }
            else
            {
                //Send updates to Holes
                Holes[m_currentHoleIndex].UpdateHole();
            }
            
        }

        #region Callbacks
        private void HoleFinished()
        {
            Holes[m_currentHoleIndex].HoleFinished -= HoleFinished;

            m_currentHoleIndex++;

            if (m_currentHoleIndex >= Holes.Length)
            {
                //We have finished this level
                this.enabled = false;

                Debug.Log("All Levels have been finished");
            }
            else
            {
                Holes[m_currentHoleIndex].StartHole(BallRef);
                Holes[m_currentHoleIndex].HoleFinished += HoleFinished;
            }
        }

        private void OnDirectorStopped(PlayableDirector DirectorStopped)
        {
            isPlayingTimeline = false;

            DirectorRef.stopped -= OnDirectorStopped;

            currentController = null;
            ControllerUpdater.instance.ActiveControllerChanged -= OnControllerChanged;

            StartLevel();
        }

        private void OnControllerChanged(ControllerBase oldController, ControllerBase newController)
        {
            currentController = newController;
        }
        #endregion
    }
}
