﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTrigger : MonoBehaviour
{
    private GameObject m_playerObj;
    private BallScript m_ballScript;

    private void Start()
    {
        //get the player object
        GameObject go = GameObject.FindWithTag("Player");
        if (go)
        {
            //save a ref of the players gameobject
            m_playerObj = go;

            //get the players ballscript
            m_ballScript = m_playerObj.GetComponent<BallScript>();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == m_playerObj)
        {
            //ensure there is a ballscript object and then call its fallout funciton.
            if (m_ballScript)
            {
                m_ballScript.fallout();
            }
        }
    }
}
