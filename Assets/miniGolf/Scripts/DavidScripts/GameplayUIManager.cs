﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CGA.Golf
{
    public class GameplayUIManager : MonoBehaviour
    {
        /// <summary>
        /// The first screen to be showing
        /// </summary>
        [SerializeField] private Screens StartingScreen;

        /// <summary>
        /// Screen displays basic information about the hole.
        /// To be shown before starting a hole
        /// </summary>
        public HoleOverviewScreen HoleOverview;

        /// <summary>
        /// Screen display minimul amount of information. 
        /// How many shots taken, Their shot limit and shot power. 
        /// To be shown during the Golf gameplay
        /// </summary>
        public HoleGameplayScreen HoleGameplay;

        /// <summary>
        /// Screen display a failure state
        /// To be shown when player failed to put ball in hole
        /// </summary>
        public HoleFailedScreen HoleFailed;

        /// <summary>
        /// Screen displays a success state
        /// Shows number of shots taken, Limit of shots and other information
        /// To be shown after completing a hole
        /// </summary>
        public HoleSuccessScreen HoleSuccess;


        /// <summary>
        /// The Current Screen being displayed
        /// </summary>
        private Screens CurrentScreen;

        /// <summary>
        /// Reference to the Current screen being displayed
        /// </summary>
        private Screen CurrentScreenRef;

        #region Monobehaviour Functions
        private void Start()
        {
            //Set all Screens to false/disabled
            HoleOverview.ScreenDisabled();
            HoleGameplay.ScreenDisabled();
            HoleFailed.ScreenDisabled();
            HoleSuccess.ScreenDisabled();

            //Set Starting screen to be enabled
            SetScreen(StartingScreen);
        }

        private void Update()
        {
            if(CurrentScreenRef != null)
            {
                CurrentScreenRef.ScreenUpdate();
            }
        }
        #endregion

        /// <summary>
        /// Change the screen to display on the UI
        /// </summary>
        /// <param name="newScreen"></param>
        public void SetScreen(Screens newScreen)
        {
            if (CurrentScreenRef != null)
            {
                CurrentScreenRef.ScreenDisabled();
            }

            CurrentScreenRef = GetScreen(newScreen);

            //Only enable the new screen if it is valid
            if(newScreen != Screens.NONE)
                CurrentScreenRef.ScreenEnabled();
            CurrentScreen = newScreen;
        }

        /// <summary>
        /// Returns a reference to the Screen requested.
        /// </summary>
        /// <param name="screen">Screen to Request for</param>
        /// <returns>Reference to requested screen. Returns null if failed</returns>
        private Screen GetScreen(Screens screen)
        {
            Screen outputScreen = null;

            switch(screen)
            {
                case Screens.HoleOverview:
                    outputScreen = HoleOverview;
                    break;

                case Screens.HoleGameplay:
                    outputScreen = HoleGameplay;
                    break;

                case Screens.HoleFailed:
                    outputScreen = HoleFailed;
                    break;

                case Screens.HoleSuccess:
                    outputScreen = HoleSuccess;
                    break;
            }

            return outputScreen;
        }

        #region Screen Base Info
        public abstract class Screen
        {
            /// <summary>
            /// The Root of the Screen UI Objects
            /// </summary>
            [Tooltip("The Root of the UI Screen")]
            public GameObject RootObject;

            /// <summary>
            /// The First object to be selected when the screen is enabled
            /// </summary>
            [Tooltip("The First UI to be selected when screen is enabled")]
            public Selectable FirstSelect;

            public abstract void ScreenEnabled();
            public abstract void ScreenDisabled();

            /// <summary>
            /// Fired when Screen Enabled or Disabled is called.
            /// </summary>
            public System.Action<bool> StateChanged;

            public abstract void ScreenUpdate();
        }

        public enum Screens
        {
            NONE,
            HoleOverview,
            HoleGameplay,
            HoleFailed,
            HoleSuccess
        }
        #endregion

        #region Screen Classes
        [System.Serializable]
        public class HoleOverviewScreen : Screen
        {
            public override void ScreenDisabled()
            {
                RootObject.SetActive(false);

                StateChanged?.Invoke(false);
            }

            public override void ScreenEnabled()
            {
                RootObject.SetActive(true);

                StateChanged?.Invoke(true);
            }

            public override void ScreenUpdate()
            {

            }
        }

        [System.Serializable]
        public class HoleGameplayScreen : Screen
        {
            public Slider PowerSlider;

            public override void ScreenDisabled()
            {
                RootObject.SetActive(false);

                StateChanged?.Invoke(false);
            }

            public override void ScreenEnabled()
            {
                RootObject.SetActive(true);

                StateChanged?.Invoke(true);

                //Start with the power slider unactive
                PowerSlider.gameObject.SetActive(false);
            }

            public override void ScreenUpdate()
            {

            }
        }

        [System.Serializable]
        public class HoleFailedScreen : Screen
        {
            public override void ScreenDisabled()
            {
                RootObject.SetActive(false);

                StateChanged?.Invoke(false);
            }

            public override void ScreenEnabled()
            {
                RootObject.SetActive(true);

                StateChanged?.Invoke(true);
            }

            public override void ScreenUpdate()
            {

            }
        }

        [System.Serializable]
        public class HoleSuccessScreen : Screen
        {
            public override void ScreenDisabled()
            {
                RootObject.SetActive(false);

                StateChanged?.Invoke(false);
            }

            public override void ScreenEnabled()
            {
                RootObject.SetActive(true);

                StateChanged?.Invoke(true);
            }

            public override void ScreenUpdate()
            {

            }
        }


        #endregion
    }
}
