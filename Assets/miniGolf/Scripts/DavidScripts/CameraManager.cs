﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private CameraStates StartingCameraStates = CameraStates.Init;

    [SerializeField] private InitCamera initCam;
    [SerializeField] private AimCamera aimCam;
    [SerializeField] private RollCamera rollCam;
    [SerializeField] private WaterCamera waterCam;
    [SerializeField] private BirdsEyeCamera birdEyeCam;

    private CameraStates currentState;

    private void Awake()
    {
        //Set all cameras to false
        initCam.m_on = false;
        aimCam.m_on = false;
        rollCam.m_on = false;
        waterCam.m_on = false;
        birdEyeCam.m_on = false;

        SetCameraOn(StartingCameraStates, true);
        currentState = StartingCameraStates;
    }

    public void SetCameraState(CameraStates newCameraState)
    {
        SetCameraOn(currentState, false);

        SetCameraOn(newCameraState, true);
        currentState = newCameraState;
    }

    private void SetCameraOn(CameraStates Camera, bool isOn)
    {
        switch (Camera)
        {
            case CameraStates.Init:
                initCam.m_on = isOn;
                break;

            case CameraStates.Aim:
                aimCam.m_on = isOn;
                break;

            case CameraStates.Roll:
                rollCam.m_on = isOn;
                break;

            case CameraStates.Water:
                waterCam.m_on = isOn;
                break;

            case CameraStates.BirdEye:
                birdEyeCam.m_on = isOn;
                break;
        }
    }

    public enum CameraStates
    {
        Init,
        Aim,
        Roll,
        Water,
        BirdEye
    }
}
