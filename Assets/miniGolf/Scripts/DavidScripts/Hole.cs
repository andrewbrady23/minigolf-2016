﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CGA.Controller;

namespace CGA.Golf
{
    public class Hole : MonoBehaviour
    {
        /// <summary>
        /// The Ball start position for this hole
        /// </summary>
        [Tooltip("The Ball start position for this hole")]
        public Transform StartPosition;

        /// <summary>
        /// The Camera Transform for Pre-Hole state. Overview shot of the Hole
        /// </summary>
        [Tooltip("The Camera Transform for Pre-Hole state. Overview shot of the Hole")]
        public Transform CameraViewTrans;

        /// <summary>
        /// The Maximum number of shots on the hole before failing the hole
        /// </summary>
        [Tooltip("The Maximum number of shots on the hole before failing the hole")]
        public int MaxNumberOfShots = 8;

        /// <summary>
        /// Reference to the hole Trigger
        /// </summary>
        [Tooltip("Reference to the Finish Hole of the Hole")]
        public VictoryTrigger HoleGoal;

        /// <summary>
        /// State the hole is in
        /// </summary>
        public HoleStates holeState { get; private set; }

        /// <summary>
        /// Called when the player finished the hole.
        /// </summary>
        public System.Action HoleFinished;

        /// <summary>
        /// Reference to the Gameplay UI
        /// </summary>
        private GameplayUIManager GameplayUI;

        /// <summary>
        /// Reference to the current controller
        /// </summary>
        private ControllerBase controllerRef;

        /// <summary>
        /// Reference to the Golf ball
        /// </summary>
        private BallScript m_BallRef;

        /// <summary>
        /// The number of shots that have been taken by the player
        /// </summary>
        private int numOfShotsTaken = 0;

        /// <summary>
        /// True if the player has scored the ball into the hole
        /// </summary>
        private bool FinishedHole = false;

        /// <summary>
        /// True if the Golf ball is charging to make a shot
        /// </summary>
        [SerializeField] private bool isChargingShot = false;

        /// <summary>
        /// Starts up the hole for use. 
        /// Subscribe to hole finished to know once the hole is finished
        /// </summary>
        public void StartHole(BallScript ballRef)
        {
            Core coreRef = Core.instance;

            //Save Ball ref
            m_BallRef = ballRef;

            //Set ball into position
            m_BallRef.transform.position = StartPosition.position;

            //Set Hole to aim for and Start Position of this hole
            m_BallRef.SetNewHole(HoleGoal.transform);
            m_BallRef.SetNewStartTrans(StartPosition);

            //Listen to ball to see if it scores and when it changes it's mode
            m_BallRef.Scored += BallScored;
            m_BallRef.BallChangedMode += BallModeChanged;
            m_BallRef.PowerChanged += BallPowerChanged;

            //Set Camera to Overview Position
            coreRef.camManager.SetCameraState(CameraManager.CameraStates.Init);
            
            //Set Hole State
            holeState = HoleStates.PreHole;

            //Set up the UI
            GameplayUI = coreRef.GameplayUI;
            GameplayUI.SetScreen(GameplayUIManager.Screens.HoleOverview);

            //Subscribe to Controller Events
            ControllerUpdater controllerManager = ControllerUpdater.instance;
            controllerRef = controllerManager.GetLastActiveController();
            controllerManager.ActiveControllerChanged += ControllerChanged;

            //Disabling the Gold Controller
            coreRef.golfController.enabled = false;

            //Reset Finsihed hole
            FinishedHole = false;
        }

        public void UpdateHole()
        {
            if (holeState == HoleStates.PreHole)
            {
                //Check if the continue button has been pressed
                if(controllerRef != null)
                {
                    if (controllerRef.GetButtonDown(controllerRef.SelectButton))
                    {
                        Core coreRef = Core.instance;

                        GameplayUI.SetScreen(GameplayUIManager.Screens.HoleGameplay);
                        holeState = HoleStates.PeriHole;

                        //Change the Camera to follow the ball
                        coreRef.camManager.SetCameraState(CameraManager.CameraStates.Aim);

                        //Set ball ready for play
                        m_BallRef.setMode(BallScript.BallMode.AIM);

                        //Give Controls to the ball
                        coreRef.golfController.enabled = true;
                    }
                }
            }
            else if (holeState == HoleStates.PeriHole)
            {
                //Any code for the Gameplay side of the hole
                if(isChargingShot)
                {
                    GameplayUI.HoleGameplay.PowerSlider.value = m_BallRef.m_currentPower;
                    Debug.LogFormat("Current Power {0}", m_BallRef.m_currentPower);
                }
            }
            else if (holeState == HoleStates.PostHole)
            {
                //Check if we have finished the hole and are ready to go onto the next hole
                if(FinishedHole)
                {
                    if(controllerRef.GetButtonDown(controllerRef.SelectButton))
                        EndHole();
                }
                else
                {
                    if (controllerRef.GetButtonDown(controllerRef.SelectButton))
                        RetryHole();
                }
            }
        }

        private void EndHole()
        {
            ///Remove all References
            //Remove Ball References
            m_BallRef.Scored -= BallScored;
            m_BallRef.BallChangedMode -= BallModeChanged;
            m_BallRef.PowerChanged -= BallPowerChanged;

            //Reset ball
            m_BallRef.Reset();

            //Lose Controller Ref
            ControllerUpdater.instance.ActiveControllerChanged -= ControllerChanged;

            //Null references
            controllerRef = null;
            m_BallRef = null;
            GameplayUI = null;

            HoleFinished?.Invoke();
        }

        private void RetryHole()
        {
            //Reset Ball to start and Set to aim mode
            m_BallRef.Reset();
            m_BallRef.transform.position = StartPosition.position;
            m_BallRef.setMode(BallScript.BallMode.AIM);

            //Reset the counter and UI
            numOfShotsTaken = 0;

            //Re-enable golf controller
            Core.instance.golfController.enabled = true;

            //Set UI to gameplay
            GameplayUI.SetScreen(GameplayUIManager.Screens.HoleGameplay);

            //Set back to Gameplay state
            holeState = HoleStates.PeriHole;
        }

        private void FailedHole()
        {
            //Disable any more golf controls
            Core.instance.golfController.enabled = false;

            holeState = HoleStates.PostHole;
            FinishedHole = false;

            GameplayUI.SetScreen(GameplayUIManager.Screens.HoleFailed);
        }

        #region Callbacks
        private void ControllerChanged(ControllerBase oldController, ControllerBase newController)
        {
            controllerRef = newController;
        }

        private void BallScored()
        {
            holeState = HoleStates.PostHole;

            //Display Victory Screen
            GameplayUI.SetScreen(GameplayUIManager.Screens.HoleSuccess);

            FinishedHole = true;

            //Disable Golf Controller
            Core.instance.golfController.enabled = false;
        }

        private void BallModeChanged(BallScript.BallMode newMode)
        {
            if(newMode == BallScript.BallMode.ACTION)
            {
                //Golf ball has been hit and increase Shots taken
                numOfShotsTaken++;

                //Update UI

                Debug.LogFormat("Number of shots taken {0} out of {1}", numOfShotsTaken, MaxNumberOfShots);
            }
            else if(newMode == BallScript.BallMode.AIM)
            {
                if(numOfShotsTaken>= MaxNumberOfShots)
                {
                    FailedHole();
                }
            }
        }

        private void BallPowerChanged(bool isPower)
        {
            GameplayUI.HoleGameplay.PowerSlider.gameObject.SetActive(isPower);

            isChargingShot = isPower;
        }
        #endregion

        /// <summary>
        /// Enum for the Different states of gameplay on a single hole
        /// </summary>
        public enum HoleStates
        {
            /// <summary>
            /// The camera is above the Hole, giving a overview of what the play is going to have to play with
            /// </summary>
            PreHole,

            /// <summary>
            /// The game is running. Player Controls the ball and the camera is following the ball
            /// </summary>
            PeriHole,

            /// <summary>
            /// The hole is over, either successly put the ball in the hole or failed to reach the par amount so the player has to try again at doing that hole
            /// </summary>
            PostHole
        }
    }
}
