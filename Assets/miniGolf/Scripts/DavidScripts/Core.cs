﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CGA.Golf
{
    public class Core : MonoBehaviour
    {
        public static Core instance
        {
            get
            {
                if(m_Instance == null)
                {
                    m_Instance = new GameObject("[Core]").AddComponent<Core>();
                }
                return m_Instance;
            }
        }
        private static Core m_Instance;

        public GameplayUIManager GameplayUI;

        public GolfController golfController;

        public CameraManager camManager;

        private void Awake()
        {
            //Making sure there can only be one
            if (m_Instance == null || m_Instance == this)
                m_Instance = this;
            else
            {
                Destroy(this);
                return;
            }
        }
    }
}
