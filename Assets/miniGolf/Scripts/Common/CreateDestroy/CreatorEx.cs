using UnityEngine;
using System.Collections;
/// <summary>
/// Creator ex will spawn a different object on the various platforms.
/// </summary>
public class CreatorEx : MonoBehaviour
{
	/// <summary>
	/// The gameObject to create if not android or IOS
	/// </summary>	
	public GameObject prefab;
	public void Awake()
	{
		if(prefab)
			Instantiate(prefab,transform.position,Quaternion.identity);
	}
}
