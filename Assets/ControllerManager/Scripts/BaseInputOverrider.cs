﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CGA.Controller
{
    public class BaseInputOverrider : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {
            StandaloneInputModule module = gameObject.GetComponent<StandaloneInputModule>();

            module.inputOverride = gameObject.AddComponent<ControllerBaseInput>();
        }
    }
}
