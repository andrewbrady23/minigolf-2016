﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CGA.Controller
{
    public class ControllerBaseInput : BaseInput
    {
        private ControllerBase currentController;

        public override float GetAxisRaw(string axisName)
        {
            if(currentController == null)
                return base.GetAxisRaw(axisName);

            Vector2 LeftStick = currentController.GetLeftStick();
            float dirX = (currentController.GetButton(ControllerBase.Buttons.Left) ? -1f : 0f) + (currentController.GetButton(ControllerBase.Buttons.Right) ? 1f : 0f),
                dirY = (currentController.GetButton(ControllerBase.Buttons.Down) ? -1f : 0f) + (currentController.GetButton(ControllerBase.Buttons.Up) ? 1f : 0f);

            if (axisName.Equals("Vertical"))
            {
                if (Mathf.Abs(LeftStick.y) > Mathf.Abs(dirY))
                    return LeftStick.y;
                else
                    return dirY;
            }

            if (axisName.Equals("Horizontal"))
            {
                if (Mathf.Abs(LeftStick.x) > Mathf.Abs(dirX))
                    return LeftStick.x;
                else
                    return dirX;
            }

            return base.GetAxisRaw(axisName);
        }

        public override bool GetButtonDown(string buttonName)
        {
            if(currentController == null)
                return base.GetButtonDown(buttonName);

            if (buttonName.Equals("Submit"))
                return currentController.GetButtonDown(currentController.SelectButton);

            if (buttonName.Equals("Cancel"))
                return currentController.GetButtonDown(currentController.BackButton);

            return base.GetButtonDown(buttonName);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            ControllerUpdater controllerRef = ControllerUpdater.instance;

            currentController = controllerRef.GetLastActiveController();

            controllerRef.ActiveControllerChanged += ControllerChanged;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            ControllerUpdater.instance.ActiveControllerChanged -= ControllerChanged;
        }

        private void ControllerChanged(ControllerBase OldController, ControllerBase NewController)
        {
            currentController = NewController;
        }
    }
}

