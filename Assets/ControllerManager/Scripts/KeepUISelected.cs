﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KeepUISelected : MonoBehaviour {

    EventSystem eventSystem;

    GameObject LastValidObject;

	// Use this for initialization
	void Start () {
        eventSystem = EventSystem.current;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(eventSystem.currentSelectedGameObject == null)
        {
            eventSystem.SetSelectedGameObject(LastValidObject);
        }
        else if(eventSystem.currentSelectedGameObject != LastValidObject)
        {
            LastValidObject = eventSystem.currentSelectedGameObject;
        }
	}
}
