﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGA.Controller
{
    public class ControllerUpdater : MonoBehaviour
    {
        #region Delegate Declaration
        public delegate void ControllerChanged(ControllerBase OldController, ControllerBase NewController);
        #endregion

        /// <summary>
        /// Eas to access reference to the Instance of the Controller Updater
        /// </summary>
        public static ControllerUpdater instance;

        public byte NumberOfPlayers = 1;

        [Tooltip("Displays the Inputs of the controller onto the screen. Only Used in Editor or Development Build")]
        public bool DisplayControllerInputs = false;

        [Tooltip("If true, will save which is the last active controller each frame and report when the last active controller has changed")]
        public bool CheckForLastActiveController = true;

        /// <summary>
        /// Fires when the last active controller changes. Will only fire if CheckForLastActiveController is true
        /// </summary>
        public ControllerChanged ActiveControllerChanged;

        private ControllerBase[] controllers;

        private bool[] validControllers;

        /// <summary>
        /// The Current Tick of this script
        /// </summary>
        private long currentTick;

        /// <summary>
        /// Index to the last active controller in the Controllers Array
        /// </summary>
        private int lastActiveController = -1;

#if UNITY_EDITOR
        [SerializeField] private EditorInput OverrideInputType = EditorInput.None;

        [System.Serializable]
        private enum EditorInput
        {
            None,
            Keyboard,
            WindowsController
        }
#endif

        private void Awake()
        {
            if (instance == null || instance == this)
                instance = this;
            else
            {
                DestroyImmediate(this);
                return;
            }

            DontDestroyOnLoad(gameObject);

            LoadControllersForPlatform();
        }

        private void LoadControllersForPlatform()
        {
#if UNITY_EDITOR
            if(OverrideInputType == EditorInput.Keyboard)
            {
                controllers = new KeyboardController[1];
                controllers[0] = new KeyboardController();
                validControllers = new bool[1];
                return;
            }
            else if(OverrideInputType == EditorInput.WindowsController)
            {
                controllers = new ControllerBase[1];
                validControllers = new bool[1];
                controllers[0] = new WindowsController(0); //Assign the Any Controller
                return;
            }
#endif

#if UNITY_SWITCH
            //Set up our Controller Type
            nn.hid.NpadId[] controllerIDs = NintendoController.InitalizeControllers(NumberOfPlayers);

            //Load in Virtual controllers 
            controllers = new ControllerBase[controllerIDs.Length];
            validControllers = new bool[controllerIDs.Length];
            for (int i = 0; i < controllers.Length; i++)
            {
                controllers[i] = new NintendoController(controllerIDs[i]);
            }
#elif UNITY_STANDALONE
            //TO DO: Create Basic PC Controller Input
            controllers = new ControllerBase[NumberOfPlayers];
            validControllers = new bool[NumberOfPlayers];

            if (NumberOfPlayers == 1)
                controllers[0] = new WindowsController(0); //Assign the Any Controller
            else
            {
                for (int i = 0; i < controllers.Length; i++)
                {
                    controllers[i] = new WindowsController(i + 1);
                }
            }
#endif
        }

        /// <summary>
        /// Updates the Controllers that are being used
        /// </summary>
        private void Update()
        {
            currentTick++;

            for (int i = 0; i < controllers.Length; i++)
            {
                validControllers[i] = controllers[i].UpdateController(currentTick);

                //if (validControllers[i]) Used for Debug testing
                //{
                //    if (controllers[i].GetButtonUp(ControllerBase.Buttons.Start))
                //        ((NintendoController)controllers[i]).ShowControllerSupport(NumberOfPlayers);
                //}
            }

            if (CheckForLastActiveController)
            {
                int index = -1;

                for (byte i = 0; i < controllers.Length; i++)
                {
                    if (index == -1 || controllers[i].LastInput > controllers[index].LastInput)
                    {
                        index = i;
                    }
                }

                if(index != -1 && lastActiveController != index)
                {
                    if (ActiveControllerChanged != null)
                        ActiveControllerChanged(lastActiveController == -1 ? null : controllers[lastActiveController], controllers[index]);

                    lastActiveController = index;
                }
            }
        }

        private ControllerBase GetPlayerController(byte PlayerID)
        {
            if (PlayerID < controllers.Length)
                return controllers[PlayerID];
            else
                return null;
        }

        public ControllerBase GetLastActiveController()
        {
            //If we are checking for the last active controller each frame then we don't need to loop through the controller to find the last active one
            if(CheckForLastActiveController)
            {
                //If a active controller has not been found yet then we return the first or null if no controllers are assigned yet
                if (lastActiveController == -1)
                {
                    if (controllers.Length > 0)
                        return controllers[0];
                    else
                        return null;
                }
                else
                    return controllers[lastActiveController];
            }
            else
            {
                int index = -1;

                for (int i = 0; i < controllers.Length; i++)
                {
                    if (index == -1 || controllers[i].LastInput > controllers[index].LastInput)
                    {
                        index = i;
                    }
                }

                if (index > -1)
                    return controllers[index];
                else if (controllers.Length > 0)
                    return controllers[0];
                else
                    return null;
            }
            
        }

        private void OnGUI()
        {
            if (!DisplayControllerInputs)
                return;

            Rect displayPos = new Rect(0f, 0f, Screen.width, Screen.height / controllers.Length); //8 Players max

            for (int i = 0; i < controllers.Length; i++)
            {
                if (!validControllers[i])
                    continue;

                GUI.Label(displayPos, controllers[i].ToString());

                displayPos.y += displayPos.height;
            }
        }
    }

    public abstract class ControllerBase
    {
        //protected Buttons AvalibleButtons; Not Sure Why I needed this?

        /// <summary>
        /// If the Controller is Plugged in and Being used
        /// </summary>
        public bool ControllerValid;

        /// <summary>
        /// The tick of the last input from the controller [Only valid for buttons]
        /// </summary>
        public long LastInput;

        /// <summary>
        /// Select button for this System (Needs to be Assigned in the Class Constructor)
        /// </summary>
        public Buttons SelectButton = Buttons.None;

        /// <summary>
        /// Back button for this System (Needs to be Assigned in the Class Constructor)
        /// </summary>
        public Buttons BackButton = Buttons.None;


        //internal abstract void InitalizeController(byte PlayerNumber, bool isSinglePlayer);
        internal abstract bool UpdateController(long tick);

        /// <summary>
        /// Returns a Vector2 of the Left Analog Stick of the Controller (-1 to 1) 
        /// </summary>
        /// <returns></returns>
        public abstract Vector2 GetLeftStick();

        /// <summary>
        /// Returns a Vector2 of the Right Analog Stick of the Controller (-1 to 1)
        /// </summary>
        /// <returns></returns>
        public abstract Vector2 GetRightStick();

        /// <summary>
        /// Returns a Vector2 of the Raw Left Analog Stick Input from the Controller (Unkown to Unknown)
        /// </summary>
        /// <returns></returns>
        public abstract Vector2 GetRawLeftStick();

        /// <summary>
        /// Returns a Vector2 of the Raw Right Analog Stick Input from the Controller (Unkown to Unknown)
        /// </summary>
        /// <returns></returns>
        public abstract Vector2 GetRawRightStick();

        public abstract bool GetButton(Buttons button);
        /// <summary>
        /// Does a OR Check on Multiple button checks
        /// </summary>
        /// <param name="buttons">Buttons to Check</param>
        /// <returns>Returns True if ANY of the buttons are true, false if all are false</returns>
        public abstract bool GetButton(Buttons[] buttons);
        public abstract bool GetButtonDown(Buttons button);
        public abstract bool GetButtonUp(Buttons button);

        public enum Buttons : int
        {
            None,
            Start = 0x1 << 0,
            Back = 0x1 << 1,
            BottomFaceButton = 0x1 << 2,
            RightFaceButton = 0x1 << 3,
            TopFaceButton = 0x1 << 4,
            LeftFaceButton = 0x1 << 5,
            Down = 0x1 << 6,
            Right = 0x1 << 7,
            Up = 0x1 << 8,
            Left = 0x1 << 9,
            RightBumper = 0x1 << 10,
            LeftBumper = 0x1 << 11,
            RightTrigger = 0x1 << 12,
            LeftTrigger = 0x1 << 13,
            LStickDown = 0x1 << 14,
            LStickRight = 0x1 << 15,
            LStickUp = 0x1 << 16,
            LStickLeft = 0x1 << 17,
            RStickDown = 0x1 << 18,
            RStickRight = 0x1 << 19,
            RStickUp = 0x1 << 20,
            RStickLeft = 0x1 << 21,
            LeftStickClick = 0x1 << 22,
            RightStickClick = 0x1 << 23
        }

        public enum Axis
        {
            None,
            LeftStickX, //This will always been the main stick to use for a single analog stick controller
            LeftStickY,
            RightStickX,
            RightStickY,
            RightTrigger,
            LeftTrigger
        }
    }
}

