﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using CGA.Controller;

public class ControllerInputModule : BaseInputModule
{
    private ControllerBase currentController;

    public override void Process()
    {
        if(currentController == null)
        {
            Debug.LogError("Current Player Controller is NULL. Can not read any controller inputs");
            return;
        }

        GameObject currentObject = eventSystem.currentSelectedGameObject;

        if (currentObject == null)
        {
            Debug.LogWarning("Current Selected Object is null. Returning, not sure how to handle this situation");
            eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject, GetBaseEventData());
            return;
        }

        Selectable currentSelect = currentObject.GetComponent<Selectable>();

        if (currentSelect == null)
        {
            Debug.LogWarning("The Current Select Object does not have a Selectable Component", currentObject);
            return;
        }

        if (currentController.GetButtonDown(ControllerBase.Buttons.Left) || currentController.GetButtonDown(ControllerBase.Buttons.LStickLeft))
        {
            Selectable newSelectable = currentSelect.FindSelectableOnLeft();

            if (newSelectable != null)
                newSelectable.Select();
        }

        if (currentController.GetButtonDown(ControllerBase.Buttons.Right) || currentController.GetButtonDown(ControllerBase.Buttons.LStickRight))
        {
            Selectable newSelectable = currentSelect.FindSelectableOnRight();

            if (newSelectable != null)
                newSelectable.Select();
        }

        if (currentController.GetButtonDown(ControllerBase.Buttons.Up) || currentController.GetButtonDown(ControllerBase.Buttons.LStickUp))
        {
            Selectable newSelectable = currentSelect.FindSelectableOnUp();

            if (newSelectable != null)
                newSelectable.Select();
        }

        if (currentController.GetButtonDown(ControllerBase.Buttons.Down) || currentController.GetButtonDown(ControllerBase.Buttons.LStickDown))
        {
            Selectable newSelectable = currentSelect.FindSelectableOnDown();

            if (newSelectable != null)
                newSelectable.Select();
        }

        if (currentController.GetButtonDown(currentController.SelectButton))
        {
            ExecuteEvents.Execute<ISubmitHandler>(currentObject, GetBaseEventData(), ExecuteEvents.submitHandler);
        }

        if (currentController.GetButtonDown(currentController.BackButton))
        {
            ExecuteEvents.Execute<ICancelHandler>(currentObject, GetBaseEventData(), ExecuteEvents.cancelHandler);
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        ControllerUpdater controllerRef = ControllerUpdater.instance;

        currentController = controllerRef.GetLastActiveController();

        controllerRef.ActiveControllerChanged += ControllerChanged;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        ControllerUpdater.instance.ActiveControllerChanged -= ControllerChanged;
    }

    private void ControllerChanged(ControllerBase OldController, ControllerBase NewController)
    {
        currentController = NewController;
    }
}
