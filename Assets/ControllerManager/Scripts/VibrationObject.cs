﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CGA.Controller;
using System;

#if UNITY_SWITCH
using nn.hid;
#endif

namespace CGA.Controller
{
    [CreateAssetMenu(fileName = "NewVibrationFile", menuName = "Controller/Vibration")]
    public class VibrationObject : ScriptableObject
    {
        public float Length;
        public float Strength;

        public VibrateOptions[] vibrations;

        public virtual IEnumerator Vibrate(ControllerBase ControllerRef, float StrengthMultiplier)
        {
#if UNITY_SWITCH
            NintendoController controller = (NintendoController)ControllerRef;

            VibrationDeviceHandle[] vibrationHandles = controller.VibrationHandles;
            int handleCount = vibrationHandles.Length;

            vibrations = SortVibrations(vibrations);

            int index = 0;
            float startTime = Time.time + vibrations[index].Time;
            float stopTime = startTime + vibrations[index].Length;

            if (vibrations[0].Time > 0.05f)
                yield return new WaitForSeconds(vibrations[0].Time);

            while (Time.time < stopTime)
            {
                

                VibrationValue vibValue = new VibrationValue(vibrations[index].LowAmplitude * StrengthMultiplier,  vibrations[index].LowFrequency,
                                                             vibrations[index].HighAmplitude * StrengthMultiplier, vibrations[index].HighFrequency);

                for (int i = 0; i < handleCount; i++)
                    nn.hid.Vibration.SendValue(vibrationHandles[i], vibValue);

                //Need to figure out how long we need to leave these controller vibrating
                
                bool needToWait;
                float timeToWait = 0f ;

                //Check if we are on the last Vibration
                if(index < vibrations.Length - 1)
                {
                    //Is the Gap Between the Current Vibration and the next vibration bigger than the length of this vibration
                    needToWait = vibrations[index + 1].Time - vibrations[index].Time > vibrations[index].Length;
                }
                else
                {
                    //If we are, then we need to wait to clear the vibration
                    needToWait = true;
                }

                if(needToWait)
                {
                    //Find the Amount of time we need to wait for
                    yield return new WaitForSeconds(vibrations[index].Length);

                    vibValue.Clear();

                    for (int i = 0; i < handleCount; i++)
                        nn.hid.Vibration.SendValue(vibrationHandles[i], vibValue);

                    if(index < vibrations.Length - 1)
                    {
                        //Find the next amount of time to wait until the next vibration
                        timeToWait = vibrations[index + 1].Time - (vibrations[index].Time + vibrations[index].Length);
                    }
                    else
                    {
                        //We are on the last vibration so we dont' need to wait
                        timeToWait = 0f;
                    }
                }
                else
                {
                    //Wait for the time to the next vibration
                    timeToWait = vibrations[index + 1].Time - vibrations[index].Time;
                }

                yield return new WaitForSeconds(timeToWait);

                //Set up for the next Vibration
                index++;

                if (index >= vibrations.Length)
                    break; //Break out if we are finished


                startTime = Time.time + vibrations[index].Time;
                stopTime = startTime + vibrations[index].Length;
            }
#elif UNITY_STANDALONE
            yield break;
#endif
        }

        private VibrateOptions[] SortVibrations(VibrateOptions[] vibrations)
        {
            if (vibrations.Length <= 0)
                return vibrations;
            
            //Vector2 element = Vector2.zero; //X - Time, Y - Index

            List<VibrateOptions> order = new List<VibrateOptions>();

            order.Add(vibrations[0]);

            for(int i = 1; i < vibrations.Length; i++)
            {
                bool vibrationSet = false;

                for (int o = 0; o < order.Count; o++)
                {
                    if(vibrations[i].Time < order[o].Time)
                    {
                        order.Insert(o, vibrations[i]);
                        vibrationSet = true;
                        break;
                    }
                }

                if (vibrationSet == false)
                    order.Add(vibrations[i]);
            }

            return order.ToArray();
        }

        [System.Serializable]
        public struct VibrateOptions
        {
            public float Time;
            public float Length;

#if UNITY_SWITCH
            [Header("Switch Only Settings")]
            public float LowAmplitude;
            [Range(40f, 640f)]
            public float LowFrequency;

            public float HighAmplitude;
            [Range(80f, 1280f)]
            public float HighFrequency;
#endif

            public VibrateOptions(bool setup)
            {
                Time = 0f;
                Length = 0f;

#if UNITY_SWITCH
                LowAmplitude = 0f;
                LowFrequency = 160f;
                HighAmplitude = 0f;
                HighFrequency = 320f;
#endif
            }
        }
    }
}
