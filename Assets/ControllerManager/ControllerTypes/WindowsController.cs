﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGA.Controller
{
    public class WindowsController : ControllerBase
    {
        private int ID;
        private string ConID;
        private const Buttons AxisMask =
            Buttons.LeftTrigger | Buttons.RightTrigger |
            Buttons.LStickUp | Buttons.LStickDown | Buttons.LStickLeft | Buttons.LStickRight |
            Buttons.RStickUp | Buttons.RStickDown | Buttons.RStickLeft | Buttons.RStickRight |
            Buttons.Up | Buttons.Down | Buttons.Left | Buttons.Right;

        private static Buttons[] ConButtons = {
            Buttons.Start, Buttons.Back,
            Buttons.BottomFaceButton, Buttons.RightFaceButton, Buttons.TopFaceButton, Buttons.LeftFaceButton,
            Buttons.Down, Buttons.Right, Buttons.Up, Buttons.Left,
            Buttons.RightBumper, Buttons.LeftBumper, Buttons.RightTrigger, Buttons.LeftTrigger,
            Buttons.LStickDown, Buttons.LStickRight, Buttons.LStickUp, Buttons.LStickLeft,
            Buttons.RStickDown, Buttons.RStickRight, Buttons.RStickUp, Buttons.RStickLeft,
            Buttons.LeftStickClick, Buttons.RightStickClick};

        private long currentInput;
        private long preInput;

        private Dictionary<Buttons, KeyCode> KeyboardControls;

        public WindowsController(int PlayerID)
        {
            ID = PlayerID;

            if (ID == 0)
                ConID = "Any";
            else
                ConID = string.Format("Con{0}", PlayerID);

            SelectButton = Buttons.BottomFaceButton;
            BackButton = Buttons.RightFaceButton;

            //string[] pads = Input.GetJoystickNames();
            //for (int i = 0; i < pads.Length; i++)
            //    Debug.Log(pads[i].ToString());

            if(ID < 2)
            {
                KeyboardControls = new Dictionary<Buttons, KeyCode>();

                KeyboardControls.Add(SelectButton, KeyCode.Return);
                KeyboardControls.Add(BackButton, KeyCode.Escape);

                KeyboardControls.Add(Buttons.Up, KeyCode.UpArrow);
                KeyboardControls.Add(Buttons.Down, KeyCode.DownArrow);
                KeyboardControls.Add(Buttons.Left, KeyCode.LeftArrow);
                KeyboardControls.Add(Buttons.Right, KeyCode.RightArrow);
            }
        }

        public override bool GetButton(Buttons button)
        {
            return (currentInput & (long)button) != 0;
        }

        public override bool GetButton(Buttons[] buttons)
        {
            Buttons mask = 0;
            //Creating multiple button mask
            for (int i = 0; i < buttons.Length; i++)
                mask = mask | buttons[i];

            return ((long)mask & currentInput) != 0;
        }

        public override bool GetButtonDown(Buttons button)
        {
            //TO DO WITH LONG
            return ((long)button & preInput) == 0 && ((long)button & currentInput) != 0;
        }

        public override bool GetButtonUp(Buttons button)
        {
            //TO DO WITH LONG
            return ((long)button & preInput) != 0 && ((long)button & currentInput) == 0;
        }

        public override Vector2 GetLeftStick()
        {
            return new Vector2(Input.GetAxis(string.Format("{0}LeftStick-X", ConID)), Input.GetAxis(string.Format("{0}LeftStick-Y", ConID)));
        }

        public override Vector2 GetRawLeftStick()
        {
            return new Vector2(Input.GetAxisRaw(string.Format("{0}LeftStick-X", ConID)), Input.GetAxisRaw(string.Format("{0}LeftStick-Y", ConID)));
        }

        public override Vector2 GetRawRightStick()
        {
            return new Vector2(Input.GetAxisRaw(string.Format("{0}RightStick-X", ConID)), Input.GetAxisRaw(string.Format("{0}RightStick-Y", ConID)));
        }

        public override Vector2 GetRightStick()
        {
            return new Vector2(Input.GetAxis(string.Format("{0}RightStick-X", ConID)), Input.GetAxis(string.Format("{0}RightStick-Y", ConID)));
        }

        internal override bool UpdateController(long tick)
        {
            preInput = currentInput;

            //Update the controller inputs
            UpdateInputs();

            return currentInput != 0;
        }

        private KeyCode ButtonToKeyCode(Buttons button)
        {
            //There are 20 buttons and the buttons IDs increment by 20 for each player ID

            switch (button)
            {
                case Buttons.Back:
                    return KeyCode.JoystickButton6 + (ID * 20);

                case Buttons.Start:
                    return KeyCode.JoystickButton7 + (ID * 20);

                #region FaceButtons
                case Buttons.RightFaceButton:
                    return KeyCode.JoystickButton1 + (ID * 20);

                case Buttons.BottomFaceButton:
                    return KeyCode.JoystickButton0 + (ID * 20);

                case Buttons.LeftFaceButton:
                    return KeyCode.JoystickButton2 + (ID * 20);

                case Buttons.TopFaceButton:
                    return KeyCode.JoystickButton3 + (ID * 20);
                #endregion

                case Buttons.LeftStickClick:
                    return KeyCode.JoystickButton8 + (ID * 20);

                case Buttons.RightStickClick:
                    return KeyCode.JoystickButton9 + (ID * 20);

                #region Triggers and Bumpers
                case Buttons.RightTrigger:
                    return KeyCode.None;

                case Buttons.LeftTrigger:
                    return KeyCode.None;

                case Buttons.LeftBumper:
                    return KeyCode.JoystickButton4 + (ID * 20);

                case Buttons.RightBumper:
                    return KeyCode.JoystickButton5 + (ID * 20);
                #endregion

                #region Directions
                case Buttons.Left:
                    return KeyCode.None;

                case Buttons.Right:
                    return KeyCode.None;

                case Buttons.Up:
                    return KeyCode.None;

                case Buttons.Down:
                    return KeyCode.None;
                #endregion

                #region Left Stick Directions
                case Buttons.LStickDown:
                    return KeyCode.None;

                case Buttons.LStickUp:
                    return KeyCode.None;

                case Buttons.LStickRight:
                    return KeyCode.None;

                case Buttons.LStickLeft:
                    return KeyCode.None;
                #endregion

                #region Right Stick Directions
                case Buttons.RStickDown:
                    return KeyCode.None;

                case Buttons.RStickUp:
                    return KeyCode.None;

                case Buttons.RStickLeft:
                    return KeyCode.None;

                case Buttons.RStickRight:
                    return KeyCode.None;
                #endregion

                default:
                    return KeyCode.None;
            }
        }

        private bool NeedToCheckAxis(Buttons button)
        {
            return (button & AxisMask) != 0;
        }

        private const float ButtonAxisThreshold = 0.25f;
        private bool GetAxisButton(Buttons button)
        {
            //Check if is a axis
            if((button & AxisMask) == 0)
            {
                Debug.LogFormat("{0} is not a axis Button");
                return false;
            }

            if(button == Buttons.LeftTrigger)
            {
                //Is not a Button, Need to check my self
                float Value = Input.GetAxis(string.Format("{0}LeftTrigger", ConID));
                return Value > ButtonAxisThreshold;
            }
            else if(button == Buttons.RightTrigger)
            {
                float Value = Input.GetAxis(string.Format("{0}RightTrigger", ConID));
                return Value > ButtonAxisThreshold;
            }
            else if(button == Buttons.LStickUp)
            {
                float Value = Input.GetAxis(string.Format("{0}LeftStick-Y", ConID));
                return Value > ButtonAxisThreshold;
            }
            else if(button == Buttons.LStickDown)
            {
                float Value = Input.GetAxis(string.Format("{0}LeftStick-Y", ConID));
                return Value < -ButtonAxisThreshold;
            }
            else if(button == Buttons.LStickLeft)
            {
                float Value = Input.GetAxis(string.Format("{0}LeftStick-X", ConID));
                return Value < -ButtonAxisThreshold;
            }
            else if(button == Buttons.LStickRight)
            {
                float Value = Input.GetAxis(string.Format("{0}LeftStick-X", ConID));
                return Value > ButtonAxisThreshold;
            }
            else if(button == Buttons.RStickUp)
            {
                float Value = Input.GetAxis(string.Format("{0}RightStick-Y", ConID));
                return Value > ButtonAxisThreshold;
            }
            else if(button == Buttons.RStickDown)
            {
                float Value = Input.GetAxis(string.Format("{0}RightStick-Y", ConID));
                return Value < -ButtonAxisThreshold;
            }
            else if(button == Buttons.RStickLeft)
            {
                float Value = Input.GetAxis(string.Format("{0}RightStick-X", ConID));
                return Value < -ButtonAxisThreshold;
            }
            else if(button == Buttons.RStickRight)
            {
                float Value = Input.GetAxis(string.Format("{0}RightStick-X", ConID));
                return Value > ButtonAxisThreshold;
            }
            else if(button == Buttons.Up)
            {
                float Value = Input.GetAxis(string.Format("{0}DPad-Y", ConID));
                return Value > ButtonAxisThreshold;
            }
            else if(button == Buttons.Down)
            {
                float Value = Input.GetAxis(string.Format("{0}DPad-Y", ConID));
                return Value < -ButtonAxisThreshold;
            }
            else if(button == Buttons.Left)
            {
                float Value = Input.GetAxis(string.Format("{0}DPad-X", ConID));
                return Value < -ButtonAxisThreshold;
            }
            else if(button == Buttons.Right)
            {
                float Value = Input.GetAxis(string.Format("{0}DPad-X", ConID));
                return Value > ButtonAxisThreshold;
            }

            return false; 
        }

        private void UpdateInputs()
        {
            currentInput = 0;

            for(int i = 0; i < ConButtons.Length; i++)
            {
                bool buttonTrue = false;

                if (NeedToCheckAxis(ConButtons[i]))
                {
                    buttonTrue = GetAxisButton(ConButtons[i]);
                }
                else
                {
                    buttonTrue = Input.GetKey(ButtonToKeyCode(ConButtons[i]));
                }

                if (buttonTrue)
                {
                    currentInput = currentInput | (long)ConButtons[i];
                }
            }

            //Check if this is controller 0/1
            if(ID < 2)
            {
                foreach(KeyValuePair<Buttons, KeyCode> element in KeyboardControls)
                {
                    if((currentInput & (long)element.Key) == 0)
                    {
                        if (Input.GetKey(element.Value))
                            currentInput = currentInput | (long)element.Key;
                    }
                }
            }
        }

        public override string ToString()
        {
            //return base.ToString();
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();

            stringBuilder.AppendFormat("ID:{0}, ConID: {1}\n", ID, ConID);

            Vector2 LeftStick = GetLeftStick(), RightStick = GetRightStick();
            stringBuilder.AppendFormat("LeftStick: x:{0}, y:{1}\nRightStick: x:{2}, y:{3}\n", LeftStick.x, LeftStick.y, RightStick.x, RightStick.y);
            stringBuilder.Append("Buttons\n");

            for(int i = 0; i < ConButtons.Length; i++)
            {
                if(GetButton(ConButtons[i]))
                {
                    stringBuilder.AppendFormat("{0}\n", ConButtons[i].ToString());
                }
            }

            return stringBuilder.ToString();
        }
    }
}
