﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGA.Controller
{
    /// <summary>
    /// This Keyboard Class is going to be janky, because Controllers do not fit well onto a keyboard. So this should be used more for Debugging purposes
    /// </summary>
    public class KeyboardController : ControllerBase
    {
        public KeyboardController()
        {
            this.BackButton = Buttons.RightFaceButton;
            this.SelectButton = Buttons.BottomFaceButton;
        }

        public override bool GetButton(Buttons button)
        {
            KeyCode keycode = ButtonToKeyCode(button);
            return Input.GetKey(keycode);
        }

        public override bool GetButton(Buttons[] buttons)
        {
            for(int i = 0; i < buttons.Length; i++)
            {
                if(Input.GetKey(ButtonToKeyCode(buttons[i])))
                {
                    return true;
                }
            }

            return false;
        }

        public override bool GetButtonDown(Buttons button)
        {
            KeyCode keycode = ButtonToKeyCode(button);
            return Input.GetKeyDown(keycode);
        }

        public override bool GetButtonUp(Buttons button)
        {
            KeyCode keycode = ButtonToKeyCode(button);
            return Input.GetKeyUp(keycode);
        }

        public override Vector2 GetLeftStick()
        {
            return GetRawLeftStick().normalized;
        }

        public override Vector2 GetRawLeftStick()
        {
            float x = (GetButton(Buttons.LStickLeft) ? -1f : 0f) + (GetButton(Buttons.LStickRight) ? 1f : 0f), y = (GetButton(Buttons.LStickDown) ? -1f : 0f) + (GetButton(Buttons.LStickUp) ? 1f : 0f);

            return new Vector2(x, y);
        }

        public override Vector2 GetRightStick()
        {
            return GetRawRightStick().normalized;
        }

        public override Vector2 GetRawRightStick()
        {
            float x = (GetButton(Buttons.RStickLeft) ? -1f : 0f) + (GetButton(Buttons.RStickRight) ? 1f : 0f), y = (GetButton(Buttons.RStickDown) ? -1f : 0f) + (GetButton(Buttons.RStickUp) ? 1f : 0f);

            return new Vector2(x, y);
        }

        internal override bool UpdateController(long tick)
        {
            if (Input.anyKey)
                LastInput = tick;

            return true;
        }

        private KeyCode ButtonToKeyCode(Buttons button)
        {
            switch (button)
            {
                case Buttons.Back:
                    return KeyCode.Minus;

                case Buttons.Start:
                    return KeyCode.Plus;

                #region FaceButtons
                case Buttons.RightFaceButton:
                    return KeyCode.Escape;

                case Buttons.BottomFaceButton:
                    return KeyCode.Return;

                case Buttons.LeftFaceButton:
                    return KeyCode.None;

                case Buttons.TopFaceButton:
                    return KeyCode.None;
                #endregion

                case Buttons.LeftStickClick:
                    return KeyCode.LeftShift;

                case Buttons.RightStickClick:
                    return KeyCode.Keypad5;

                #region Triggers and Bumpers
                case Buttons.RightTrigger:
                    return KeyCode.Mouse1;

                case Buttons.LeftTrigger:
                    return KeyCode.Mouse0;

                case Buttons.LeftBumper:
                    return KeyCode.Alpha1;

                case Buttons.RightBumper:
                    return KeyCode.Alpha2;
                #endregion

                #region Directions
                case Buttons.Left:
                    return KeyCode.LeftArrow;

                case Buttons.Right:
                    return KeyCode.RightArrow;

                case Buttons.Up:
                    return KeyCode.UpArrow;

                case Buttons.Down:
                    return KeyCode.DownArrow;
                #endregion

                #region Left Stick Directions
                case Buttons.LStickDown:
                    return KeyCode.S;

                case Buttons.LStickUp:
                    return KeyCode.W;

                case Buttons.LStickRight:
                    return KeyCode.D;

                case Buttons.LStickLeft:
                    return KeyCode.A;
                #endregion

                #region Right Stick Directions
                case Buttons.RStickDown:
                    return KeyCode.Keypad2;

                case Buttons.RStickUp:
                    return KeyCode.Keypad8;

                case Buttons.RStickLeft:
                    return KeyCode.Keypad4;

                case Buttons.RStickRight:
                    return KeyCode.Keypad6;
                #endregion

                default:
                    return KeyCode.None;
            }
        }
    }
}
