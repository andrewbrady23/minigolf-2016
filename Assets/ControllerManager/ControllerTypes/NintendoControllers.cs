﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using nn.hid;
using System;

#if UNITY_SWITCH
namespace CGA.Controller
{
    public class NintendoController : ControllerBase
    {
        #region Delegate Declariation
        public delegate void StyleChanged(NintendoController ControllerRef, NpadStyle OldStyle, NpadStyle NewStyle);
        #endregion
        //Pro Controller - Full Controller Assignments make sense
        //private Buttons fullControllerButtons =
        //    Buttons.BottomFaceButton | Buttons.RightFaceButton | Buttons.TopFaceButton | Buttons.LeftFaceButton |
        //    Buttons.Left | Buttons.Up | Buttons.Right | Buttons.Down |
        //    Buttons.Start | Buttons.Back |
        //    Buttons.RightBumper | Buttons.LeftBumper |
        //    Buttons.RightTrigger | Buttons.LeftTrigger |
        //    Buttons.LeftStickClick | Buttons.RightStickClick |
        //    Buttons.LeftStickLeft | Buttons.LeftStickUp | Buttons.LeftStickRight | Buttons.LeftStickDown |
        //    Buttons.RightStickUp | Buttons.RightStickRight | Buttons.RightStickDown | Buttons.RightStickLeft;

        //Single Joy Con - Left Stick is only Analog stick. 
        //private Buttons singleJoyConButtons =
        //    Buttons.BottomFaceButton | Buttons.RightFaceButton | Buttons.TopFaceButton | Buttons.LeftFaceButton |
        //    Buttons.LeftStickLeft | Buttons.LeftStickUp | Buttons.LeftStickRight | Buttons.LeftStickDown | Buttons.LeftStickClick |
        //    Buttons.Start;

        /// <summary>
        /// Current Frames Input value in a long
        /// </summary>
        private long currentInput;

        /// <summary>
        /// Previous Frames input value in a long
        /// </summary>
        private long preInput;

        private bool isFullController { get { return ControllerType == NpadStyle.FullKey || ControllerType == NpadStyle.Handheld || ControllerType == NpadStyle.JoyDual; } }

        private NpadState padState;

        private NpadStyle ControllerType = NpadStyle.None;
        private NpadId ControllerID = NpadId.Invalid;

        private VibrationDeviceHandle[] m_VibrationHandles = null;

        public NpadId PadID { get { return ControllerID; } }
        public NpadStyle PadStyle { get { return ControllerType; } }

        public VibrationDeviceHandle[] VibrationHandles { get { return m_VibrationHandles; } }

        public StyleChanged PadStyleChanged;

        public NintendoController(NpadId ControllerID)
        {
            this.ControllerID = ControllerID;

            this.SelectButton = Buttons.RightFaceButton;
            this.BackButton = Buttons.BottomFaceButton;

            this.PadStyleChanged += UpdateVibrationHandles;
        }

        internal override bool UpdateController(long tick)
        {
            NpadStyle style = Npad.GetStyleSet(ControllerID);

            if(style != ControllerType)
            {
                NpadStyle oldStyle = ControllerType;
                ControllerType = style;

                if (PadStyleChanged != null)
                    PadStyleChanged(this, oldStyle, style);
            }

            ControllerValid = ControllerType != NpadStyle.None;

            if (!ControllerValid)
                return false;

            Npad.GetState(ref padState, ControllerID, ControllerType);

            preInput = currentInput;

            currentInput = (long)padState.buttons;

            if (currentInput > 0)
                LastInput = tick;

            return true;
        }

        private void UpdateVibrationHandles(NintendoController ControllerRef, NpadStyle OldStyle, NpadStyle NewStyle)
        {
            int vibrationCount;

            switch (ControllerType)
            {
                case NpadStyle.FullKey:
                case NpadStyle.Handheld:
                case NpadStyle.JoyDual:
                    vibrationCount = 2;
                    break;

                case NpadStyle.JoyLeft:
                case NpadStyle.JoyRight:
                    vibrationCount = 1;
                    break;

                case NpadStyle.Invalid:
                case NpadStyle.None:
                default:
                    vibrationCount = 0;
                    break;
            }

            Debug.LogFormat("Vibration Handles of {0}", vibrationCount);

            m_VibrationHandles = new VibrationDeviceHandle[vibrationCount]; //2 is the Maximum amount of handles? 

            if (vibrationCount == 0)
                return; //Nothing that can be set up

            int controllersCount = nn.hid.Vibration.GetDeviceHandles(m_VibrationHandles, vibrationCount, ControllerID, ControllerType);

            Debug.LogFormat("Controller Count is {0}", controllersCount);

            for (int i = 0; i < m_VibrationHandles.Length; i++)
            {
                nn.hid.Vibration.InitializeDevice(m_VibrationHandles[i]);
                //Unsure of the use for the Device Info just yet, but it is there if needed
                //VibrationDeviceInfo DeviceInfo = new VibrationDeviceInfo(); Get Device info like left and right
                //Vibration.GetDeviceInfo(ref DeviceInfo, VibrationHandles[i]);
            }

            Debug.Log("Initalized all Handles");
        }

        #region Static Functions
        public static NpadId[] InitalizeControllers(int NumberOfPlayers)
        {
            Npad.Initialize();

            NpadId[] ControllerIDs = SetNumberOfControllers(NumberOfPlayers);

            SetSupportedControllerType(true);

            NpadJoy.SetHoldType(NpadJoyHoldType.Horizontal);

            // Used for single joycon testing
            //for (int i = 0; i < ControllerIDs.Length; i++) 
            //    NpadJoy.SetAssignmentModeSingle(ControllerIDs[i]);

            return ControllerIDs;
        }

        private static NpadId[] SetNumberOfControllers(int NumberOfControllers)
        {
            NpadId[] ControllerIDs = new NpadId[NumberOfControllers + 1]; //+1 for handheld
            ControllerIDs[0] = NpadId.Handheld;
            for (int i = 1; i < ControllerIDs.Length; i++)
            {
                ControllerIDs[i] = (NpadId)i - 1;
            }

            Npad.SetSupportedIdType(ControllerIDs);

            return ControllerIDs;
        }

        private static void SetSupportedControllerType(bool SupportSingleJoyCons)
        {
            NpadStyle SupportedStyles = NpadStyle.FullKey | NpadStyle.Handheld | NpadStyle.JoyDual;
            if (SupportSingleJoyCons)
                SupportedStyles |= NpadStyle.JoyLeft | NpadStyle.JoyRight;

            Npad.SetSupportedStyleSet(SupportedStyles);
        }
        #endregion

        #region Get Button Down and Up
        public override bool GetButton(Buttons button)
        {
            NpadButton controllerButton = ButtonToNPad(button);
            //Return if it down or not
            return (currentInput & (long)controllerButton) != 0;
        }

        public override bool GetButton(Buttons[] buttons)
        {
            long allButtons = 0;

            for (int i = 0; i < buttons.Length; i++)
                allButtons += (long)ButtonToNPad(buttons[i]);

            return (currentInput & allButtons) != 0;
        }

        /// <summary>
        /// None Override Functions for Pure NPad button to read [Quicker than Genric Get Button Function]
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButton(NpadButton button)
        {
            return (currentInput & (long)button) != 0;
        }

        public override bool GetButtonDown(Buttons button)
        {
            NpadButton controllerButton = ButtonToNPad(button);
            //If the Previous Frame is False, and Current Frame is true
            return (preInput & (long)controllerButton) == 0 && (currentInput & (long)controllerButton) != 0;
        }

        /// <summary>
        /// None Override Functions for Pure NPad button to read [Quicker than Genric GetButtonDown Function]
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonDown(NpadButton button)
        {
            //If the Previous Frame is False, and Current Frame is true
            return (preInput & (long)button) == 0 && (currentInput & (long)button) != 0;
        }

        public override bool GetButtonUp(Buttons button)
        {
            NpadButton controllerButton = ButtonToNPad(button);

            //Was true for last frame and is now false
            return (preInput & (long)controllerButton) != 0 && (currentInput & (long)controllerButton) == 0;
        }

        /// <summary>
        /// None Override Functions for Pure NPad button to read [Quicker than Genric GetButtonUp Function]
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool GetButtonUp(NpadButton button)
        {
            //Was true for last frame and is now false
            return (preInput & (long)button) != 0 && (currentInput & (long)button) == 0;
        }
        #endregion

        private NpadButton ButtonToNPad(Buttons button)
        {
            switch (button)
            {
                case Buttons.Back:
                    return isFullController ? NpadButton.Minus : NpadButton.None;

                case Buttons.Start:
                    return isFullController || ControllerType == NpadStyle.JoyRight ? NpadButton.Plus : NpadButton.Minus;

                #region FaceButtons
                case Buttons.RightFaceButton:
                    return isFullController ? NpadButton.A :
                        ControllerType == NpadStyle.JoyRight ? NpadButton.X : NpadButton.Down;

                case Buttons.BottomFaceButton:
                    return isFullController ? NpadButton.B :
                        ControllerType == NpadStyle.JoyRight ? NpadButton.A : NpadButton.Left;

                case Buttons.LeftFaceButton:
                    return isFullController ? NpadButton.Y :
                        ControllerType == NpadStyle.JoyRight ? NpadButton.B : NpadButton.Up;

                case Buttons.TopFaceButton:
                    return isFullController ? NpadButton.X :
                        ControllerType == NpadStyle.JoyRight ? NpadButton.Y : NpadButton.Right;
                #endregion

                case Buttons.LeftStickClick:
                    return isFullController || ControllerType == NpadStyle.JoyLeft ? NpadButton.StickL : NpadButton.StickR;

                case Buttons.RightStickClick:
                    return isFullController ? NpadButton.StickR : NpadButton.None;

                #region Triggers and Bumpers
                case Buttons.RightTrigger:
                    return isFullController ? NpadButton.ZR :
                        ControllerType == NpadStyle.JoyLeft ? NpadButton.LeftSR : NpadButton.RightSR;

                case Buttons.LeftTrigger:
                    return isFullController ? NpadButton.ZL :
                        ControllerType == NpadStyle.JoyLeft ? NpadButton.LeftSL : NpadButton.RightSL;

                case Buttons.LeftBumper:
                    return isFullController ? NpadButton.L : NpadButton.None;

                case Buttons.RightBumper:
                    return isFullController ? NpadButton.R : NpadButton.None;
                #endregion

                #region Directions
                case Buttons.Left:
                    return isFullController ? NpadButton.Left : NpadButton.None;

                case Buttons.Right:
                    return isFullController ? NpadButton.Right : NpadButton.None;

                case Buttons.Up:
                    return isFullController ? NpadButton.Up : NpadButton.None;

                case Buttons.Down:
                    return isFullController ? NpadButton.Down : NpadButton.None;
                #endregion

                #region Left Stick Directions
                case Buttons.LStickDown:
                    return isFullController ? NpadButton.StickLDown :
                        ControllerType == NpadStyle.JoyLeft ? NpadButton.StickLLeft : NpadButton.StickRRight;

                case Buttons.LStickUp:
                    return isFullController ? NpadButton.StickLUp :
                        ControllerType == NpadStyle.JoyLeft ? NpadButton.StickLRight : NpadButton.StickRLeft;

                case Buttons.LStickRight:
                    return isFullController ? NpadButton.StickLRight :
                        ControllerType == NpadStyle.JoyLeft ? NpadButton.StickLDown : NpadButton.StickRUp;

                case Buttons.LStickLeft:
                    return isFullController ? NpadButton.StickLLeft :
                        ControllerType == NpadStyle.JoyLeft ? NpadButton.StickLUp : NpadButton.StickRDown;
                #endregion

                #region Right Stick Directions
                case Buttons.RStickDown:
                    return isFullController ? NpadButton.StickRDown : NpadButton.None;

                case Buttons.RStickUp:
                    return isFullController ? NpadButton.StickRUp : NpadButton.None;

                case Buttons.RStickLeft:
                    return isFullController ? NpadButton.StickRLeft : NpadButton.None;

                case Buttons.RStickRight:
                    return isFullController ? NpadButton.StickRRight : NpadButton.None;
                #endregion

                default:
                    return NpadButton.None;
            }
        }

        #region Get Stick Inputs
        public override Vector2 GetLeftStick()
        {
            if (isFullController)
                return new Vector2(padState.analogStickL.fx, padState.analogStickL.fy);
            else if (ControllerType == NpadStyle.JoyLeft)
                //Left Joy Con
                return new Vector2(-padState.analogStickL.fy, padState.analogStickL.fx);
            else
                //Right Joy Con
                return new Vector2(padState.analogStickR.fy, -padState.analogStickR.fx);
        }

        public override Vector2 GetRawLeftStick()
        {
            if (isFullController)
                return new Vector2(padState.analogStickL.x, padState.analogStickL.y);
            else if (ControllerType == NpadStyle.JoyLeft)
                //Left Joy Con
                return new Vector2(-padState.analogStickL.y, padState.analogStickL.x);
            else
                //Right Joy Con
                return new Vector2(padState.analogStickR.y, -padState.analogStickR.x);
        }

        public override Vector2 GetRightStick()
        {
            if (isFullController)
                return new Vector2(padState.analogStickR.fx, padState.analogStickR.fy);
            else
                return Vector2.zero;
        }

        public override Vector2 GetRawRightStick()
        {
            if (isFullController)
                return new Vector2(padState.analogStickR.x, padState.analogStickR.y);
            else
                return Vector2.zero;
        }
        #endregion

        public override string ToString()
        {
            System.Text.StringBuilder output = new System.Text.StringBuilder();

            output.AppendFormat("ControllerID: {0}", ControllerID.ToString());

            output.AppendFormat("Left Stick: {0}, ", GetLeftStick());

            if (isFullController)
                output.AppendFormat("Right Stick: {0}, ", GetRightStick());

            output.Append("Buttons Down: ");

            var enums = System.Enum.GetValues(typeof(Buttons));

            foreach (Buttons button in enums)
            {
                if (GetButton(button))
                {
                    output.AppendFormat("{0}, ", button.ToString());
                }
            }

            return output.ToString();
        }

        //DEBUG STUFF
        private ControllerSupportArg controllerSupportArg = new ControllerSupportArg();
        private nn.Result result = new nn.Result();

        public void ShowControllerSupport(int maxControllers)
        {
            controllerSupportArg.SetDefault();
            controllerSupportArg.playerCountMax = (byte)(maxControllers - 1);

            controllerSupportArg.enableIdentificationColor = true;
            controllerSupportArg.identificationColor[0].Set(255, 128, 128, 255); //Setting the Color for Each player section
            controllerSupportArg.identificationColor[1].Set(128, 128, 255, 255);
            controllerSupportArg.identificationColor[2].Set(128, 255, 128, 255);
            controllerSupportArg.identificationColor[3].Set(224, 224, 128, 255);

            controllerSupportArg.enableExplainText = true;
            ControllerSupport.SetExplainText(ref controllerSupportArg, "Red", NpadId.No1); //Setting the Name of each player in the Applet
            ControllerSupport.SetExplainText(ref controllerSupportArg, "Blue", NpadId.No2);
            ControllerSupport.SetExplainText(ref controllerSupportArg, "Green", NpadId.No3);
            ControllerSupport.SetExplainText(ref controllerSupportArg, "Yellow", NpadId.No4);

            Debug.Log(controllerSupportArg);
            result = ControllerSupport.Show(controllerSupportArg);
            if (!result.IsSuccess()) { Debug.Log(result); }
        }
    }
}
#endif
